#include "scanner.ih"

void Scanner::startRadix()
{
    d_radix = stoul(matched());
    d_value = 0;
    d_radixOK = false;

    d_legalChars = Value::numberChars(d_radix);

    begin(StartCondition_::radix);
}

