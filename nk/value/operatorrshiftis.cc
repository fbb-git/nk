#include "value.ih"

Value &Value::operator>>=(Value const &right)
{
    deref().binop(RSHIFT, right.deref());
    return *this;
}
