#include "parser.ih"

void Parser::resetLine()
{
    if (d_line.size())
        d_line.resize(d_line.size() - 1);           // rm \n

    d_line.erase();
    d_begin = 0;
    d_end = 0;
}
