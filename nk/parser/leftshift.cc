#include "parser.ih"

// support functions, missing in <functional>
Value Parser::leftShift(Value const &left, Value const &right)
{
    return left << right;
}

