#include "identvalue.ih"

ValueBase *IdentValue::clone() const
{
    return vDeref()->valueBase().copy();
}
