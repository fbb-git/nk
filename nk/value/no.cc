#include "value.ih"

Value const &Value::no(char const *operation, Value &lhs, Value &rhs,
                                                          Value const &right)
{
    throw Exception() << 
        "`" << lhs.typeName() << ' ' << operation << ' ' << 
                                        right.typeName() << "' not defined";
    return right;
}
