#include "function.ih"

double Function::differenceCoord(double first, double second)
{
    double difference = fabs(first - second);

    return difference > 180 ?           // make sure max. diff is 180 deg.
                360 - difference
            :
                difference;
}
