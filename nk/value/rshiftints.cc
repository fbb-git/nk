#include "value.ih"

long long Value::rshiftInts(Value const &lhs, Value const &rhs)
{
    return lhs.longint() >> rhs.longint();
}
