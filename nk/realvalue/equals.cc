#include "realvalue.ih"

bool RealValue::equals(ValueBase const &rhs) const
{
    return rhs.type() == REAL && value() == rhs.real();
}
