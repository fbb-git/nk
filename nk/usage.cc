//                     usage.cc

#include "main.ih"

void usage(std::string const &progname)
{
    cerr << "\n" <<
    progname << " by " << Icmbuild::author << "\n" <<
    progname << " V" << Icmbuild::version << " " << Icmbuild::years << "\n"
    "\n"
    "Usage: " << progname << " [options] args\n"
    "Where:\n"
    "   [options] - optional arguments (short options and default values "
                                                                "between\n"
    "               parentheses):\n"
    "   -h (--help)     - show some initial help-information, the calculator "
                                                                "is\n"
    "                  not started\n"
    "   -r (--read) <filename>   - read the file <filename> on startup. "
                                                                "<filename>\n"
    "                  is optional; if <filename> is omitted, but -r is "
                                                                "specified\n"
    "                  ~/.navrc is read\n"
    "   -t (--tokens)   - show the tokens returned by the lexical scanner\n"
    "   -v (--version)  - show navcalc's version and end navcalc.\n"
    "   -w (--write) <filename>   - write the file <filename> when the \n"
    "                  navcalc ends. <filename> is optional;\n"
    "                  if <filename> is omitted, but -r is specified\n"
    "                  ~/.navrc is (re)written\n"
    "\n"
    ;
}

