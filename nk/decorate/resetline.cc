#include "parser.ih"

void Parser::resetLine()
{
    d_line.resize(d_line.size() - 1);           // rm \n

    d_line.erase();
    d_begin = 0;
    d_end = 0;
}
