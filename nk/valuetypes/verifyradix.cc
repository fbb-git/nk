#include "valuetypes.ih"

void ValueTypes::verifyRadix(size_t radix)
{
    if (radix < 2 || maxRadix() < radix)
        throw Exception() << 
                "number system base must be within the range 2.." << 
                maxRadix();
}
