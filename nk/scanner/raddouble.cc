#include "scanner.ih"

int Scanner::radDouble()
{
    d_value = stod(matched());
    return matched().back() == 'r' ? Parser::RAD : Parser::REAL;
}
