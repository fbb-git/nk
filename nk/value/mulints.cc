#include "value.ih"

long long Value::mulInts(Value const &lhs, Value const &rhs)
{
    return lhs.longint() * rhs.longint();
}
