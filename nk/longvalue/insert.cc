#include "longvalue.ih"

ostream &LongValue::insert(ostream &out) const
{
    return toLong(out, value());
}
