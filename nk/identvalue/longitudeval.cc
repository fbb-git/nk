#include "identvalue.ih"

double IdentValue::longitudeVal() const
{
    return vDeref()->longitude();
}
