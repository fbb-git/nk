#include "valuebase.ih"

ostream &ValueBase::toLat(ostream &out, double value)
{
    return toDegMinSec(out, fabs(value)) << ' ' << 
                    (value >= 0 ? 'N' : 'S');
}
