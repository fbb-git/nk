#include "doublebase.ih"

double DoubleBase::latitudeVal() const
{
    return normalize(LAT, d_value);
}
