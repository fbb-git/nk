#include "funwrap.ih"

Value FunWrap::operator()(size_t nArgs, Value *args) const
{
    return d_base->operator()(nArgs, args);
}
        
