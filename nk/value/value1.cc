#include "value.ih"

Value::Value(Value const &other)
:
    d_value(other.d_value->copy())
{}
