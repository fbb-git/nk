#include "degvalue.ih"

bool DegValue::equals(ValueBase const &rhs) const
{
    return rhs.type() == DEG && value() == rhs.real();
}
