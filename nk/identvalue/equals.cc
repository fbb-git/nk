#include "identvalue.ih"

bool IdentValue::equals(ValueBase const &rhs) const
{
    return vDeref()->valueBase() == rhs;
}
