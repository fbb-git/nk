#ifndef INCLUDED_RADVALUE_
#define INCLUDED_RADVALUE_

#include <iosfwd>
#include "../doublebase/doublebase.h"

class RadValue: public DoubleBase
{
    public:
        explicit RadValue(double value = 0);   // 1 default constructor

    private:
        std::ostream &insert(std::ostream &out) const override;
        bool equals(ValueBase const &rhs) const override;

        using ValueBase::latitudeVal;           // these are now suppressed
        using ValueBase::longitudeVal;

        ValueBase *clone() const override;
};

#endif





