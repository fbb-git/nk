#include "realvalue.ih"

ostream &RealValue::insert(ostream &out) const
{
    return out << value();
}
