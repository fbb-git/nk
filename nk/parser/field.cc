#include "parser.ih"

ExprData Parser::field(ExprData &fix, Value::Type type)
{
    Value value = rvalue(fix);

    if (value.type() != Value::FIX)
        throw Exception() << ".lat/.long only with fix-type values";

    return ExprData(
                Value::factory(type, type == Value::LAT ? value.latitude() :
                                                        value.longitude()),
                fix.begin(), d_end, 
                fix.variables()
            );
}
