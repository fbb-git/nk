#include "parser.ih"

ExprData Parser::value(Value::Type type) const
{
    return ExprData(Value::factory(type, d_scanner.value()), d_begin, d_end,
                                    false);
}
