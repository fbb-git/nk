#include "parser.ih"

void Parser::assign(ExprData &lhs, ExprData const &rhs)
{
    if (lhs.type() != Value::IDENT)
        err("lvalue must be variable");

    cout << "ASSIGN: " << rhs.value() << " = " <<
            rhs.variables()  << ": " << 
            d_line.substr(rhs.begin(), rhs.length()) << endl;

    lhs.assign(rhs);
    lhs.symData().set(rhs.variables() ?
                            d_line.substr(rhs.begin(), rhs.length())
                        :
                            ""
                    );        
}
