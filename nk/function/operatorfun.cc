#include "function.ih"

Value Function::operator()(string const &name, size_t nArgs, Value *args) 
                                                                        const
{
    Value ret;

    auto iter = s_function.find(name);

    if (iter == s_function.end())
        throw Exception() << "function `" << name << "' not implemented";

    ret = (iter->second)(nArgs, args);  // funbase operator() (operatorfun.cc)

    return ret;
}



