#include "valuebase.ih"

long long ValueBase::intVal() const
{
    wrongType("int");
    return 0;
}
