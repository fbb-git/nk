#ifndef INCLUDED_IDENTVALUE_
#define INCLUDED_IDENTVALUE_

#include <iosfwd>

#include "../valuebase/valuebase.h"

#include "../symtab/symtab.h"

//class Value;
class IdentValue: public ValueBase
{
//    Value *d_value;
    Symtab::Data *d_ident;

    public:
        explicit IdentValue(Symtab::Data *identData);   // Value &value);

    private:
        Value *vDeref() const override;

        long long intVal() const override;
        double realVal() const override;  
        double latitudeVal() const override;
        double longitudeVal() const override;

        Type vType() const override;

        std::ostream &insert(std::ostream &out) const override;
        bool equals(ValueBase const &rhs) const override;

        ValueBase *clone() const override;
};

#endif





