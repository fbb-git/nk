#include "parser.ih"

int Parser::lex()
try
{
    if (d_preToken)
    {
        int token = d_preToken;
        d_preToken = 0;
        return token;
    }

    int token = d_scanner.lex();

    d_begin = d_end;
    d_end += d_scanner.length();
    d_line += d_scanner.matched();

    return token;
}
catch (exception const &exc)
{
    cout << exc.what() << '\n';
    ERROR();
    return 0;       // not reached
}
