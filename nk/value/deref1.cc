#include "value.ih"

Value &Value::deref()
{   
    return *(isLvalue() ? d_value->deref() : this);
}
