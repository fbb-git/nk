#ifndef INCLUDED_PREINCLUDES_H_
#define INCLUDED_PREINCLUDES_H_

#include <vector>
#include "../value/value.h"
#include "../exprdata/exprdata.h"

struct ArgType
{
    std::vector<Value> arg;
    bool               variables = false;

    ArgType() = default;
    ArgType(Value const &value, bool vars);
};

struct TypeData
{
    size_t begin;
    Value::Type type;
};
        

#endif
