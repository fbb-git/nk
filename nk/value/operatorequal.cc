#include "value.ih"

bool operator==(Value const &lhs, Value const &rhs)
{
    return *lhs.d_value == *rhs.d_value;
}
