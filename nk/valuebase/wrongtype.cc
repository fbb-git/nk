#include "valuebase.ih"

void ValueBase::wrongType(char const *type) const
{
    throw Exception() << typeName(d_type) << ": no conversion to " << type;
}
