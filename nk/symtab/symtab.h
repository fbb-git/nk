#ifndef INCLUDED_SYMTAB_
#define INCLUDED_SYMTAB_

#include <string>
#include <iosfwd>
#include <memory>
#include <unordered_map>

#include "../value/value.h"
#include "../function/function.h"

class Value;

struct Symtab
{
    class Data
    {
        Value           d_value;
        std::string     d_exprText;
        size_t          d_nested;

        public:
            Data();

            std::string const &exprText() const;
            bool hasExprText() const;
            bool nested() const;

            void pushNested();
            void popNested();

            void set(Value &&tmp);

            Value &value();
            Value const &value() const;

            void set(std::string const &exprText);
    };
        
    typedef std::unordered_map<std::string, std::unique_ptr<Data>>  
                                        Hashtab;
    typedef Hashtab::const_iterator     const_iterator;
    typedef Hashtab::value_type         HashtabValue;

        
    private:
        Function d_function;    // all predefined functions;
        Hashtab d_entry;

    public:
                                            // the args are non-const to
                                            // to allow type conversions for
                                            // functions.
        Value operator()(std::string const &name, size_t nArgs, Value *args) 
                                                                        const;
        Data &operator[](std::string const &name);
        const_iterator begin() const;
        const_iterator end() const;

        bool isFunction(HashtabValue const &entry) const;

        void clear();
        void list() const;

    private:
        static bool cmpNames(HashtabValue const *first, 
                            HashtabValue const *second);
};

std::ostream &operator<<(std::ostream &out, 
                         Symtab::HashtabValue const &value);

inline bool Symtab::isFunction(HashtabValue const &entry) const
{
    return d_function.isFunction(entry.first);
}

inline std::ostream &operator<<(std::ostream &out, 
                                    Symtab::const_iterator const &iter)
{
    return out << *iter;
}

inline Symtab::const_iterator Symtab::begin() const
{
    return d_entry.begin();
}

inline Symtab::const_iterator Symtab::end() const
{
    return d_entry.end();
}

inline std::string const &Symtab::Data::exprText() const
{
    return d_exprText;
}

inline bool Symtab::Data::hasExprText() const
{
    return not d_exprText.empty();
}

inline bool Symtab::Data::nested() const
{
    return d_nested;
}

inline void Symtab::Data::pushNested()
{
    ++d_nested;
}

inline void Symtab::Data::popNested()
{
    --d_nested;
}

inline Value const &Symtab::Data::value() const
{
    return d_value;
}

inline Value &Symtab::Data::value()
{
    return d_value;
}

inline void Symtab::Data::set(Value &&tmp)
{
    d_value = std::move(tmp);
}

inline void Symtab::Data::set(std::string const &exprText)
{
    d_exprText = exprText;
}

#endif



