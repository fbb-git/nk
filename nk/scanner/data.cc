#include "scanner.ih"

Scanner::CommandType const Scanner::s_commands[] =
{
    {   "/deg",     Parser::DEG,        2},
    {   "/fix",     Parser::FIX,        2},
    {   "/grad",    Parser::GRAD,       2},
    {   "/help",    Parser::HELP,       2},
    {   "/list",    Parser::LIST,       2},
    {   "/quit",    Parser::QUIT,       2},
    {   "/rad",     Parser::RADIANS,    3},
    {   "/read",    Parser::READ,       3},
    {   "/write",   Parser::WRITE,      2},
};

Scanner::CommandType const *const Scanner::s_commandsEnd = 
                    s_commands + sizeof(s_commands) / sizeof(s_commands[0]);
