#include "symtab.ih"

void Symtab::list() const
{
    if (d_entry.empty())
    {
        cout << "empty symbol table\n";
        return;
    }

    vector<HashtabValue const *> support;
    for (auto &value: d_entry)
    {
        if (not d_function.isFunction(value.first))
            support.push_back(&value);
    }

    sort(support.begin(), support.end(), cmpNames);

    for (auto &entry: support)
        cout << *entry << '\n';
}







