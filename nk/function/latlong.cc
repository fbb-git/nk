#include "function.ih"

FixValue::LatLong Function::latLong(Value const *args, int)
{
    return FixValue::LatLong {args[0].latitude(), args[1].longitude()};
}

