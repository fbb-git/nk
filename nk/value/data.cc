#include "value.ih"

std::unordered_map<int, Value::CastStruct> Value::s_cast =
{
    {'&', {"&", andInts, 0,
            {
  // lhs:     |----------------------- rhs ----------------------------|  
  //          INT ,     REAL ,    DEG ,  RAD , GRAD,  LAT ,  LONG , FIX ,  
  /* INT  */ { ok,      no,      no,      no,   no,   no,    no,    no, },
  /* REAL */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* DEG  */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* RAD  */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* GRAD  */{ no,      no,      no,      no,   no,   no,    no,    no, },
  /* LAT  */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* LONG */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* FIX  */ { no,      no,      no,      no,   no,   no,    no,    no, },
            }
        }
    },

    {'|', {"|", orInts, 0,
            {
  // lhs:     |----------------------- rhs ----------------------------|  
  //          INT ,     REAL ,    DEG ,  RAD , GRAD,  LAT ,  LONG , FIX ,  
  /* INT  */ { ok,      no,      no,      no,   no,   no,    no,    no, },
  /* REAL */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* DEG  */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* RAD  */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* GRAD  */{ no,      no,      no,      no,   no,   no,    no,    no, },
  /* LAT  */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* LONG */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* FIX  */ { no,      no,      no,      no,   no,   no,    no,    no, },
            }
        }
    },

    {'^', {"^", xorInts, 0,
            {
  // lhs:     |----------------------- rhs ----------------------------|  
  //          INT ,     REAL ,    DEG ,  RAD , GRAD,  LAT ,  LONG , FIX ,  
  /* INT  */ { ok,      no,      no,      no,   no,   no,    no,    no, },
  /* REAL */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* DEG  */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* RAD  */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* GRAD  */{ no,      no,      no,      no,   no,   no,    no,    no, },
  /* LAT  */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* LONG */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* FIX  */ { no,      no,      no,      no,   no,   no,    no,    no, },
            }
        }
    },

    {LSHIFT, {"<<", lshiftInts, 0,
            {
  // lhs:     |----------------------- rhs ----------------------------|  
  //          INT ,     REAL ,    DEG ,  RAD , GRAD,  LAT ,  LONG , FIX ,  
  /* INT  */ { ok,      no,      no,      no,   no,   no,    no,    no, },
  /* REAL */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* DEG  */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* RAD  */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* GRAD  */{ no,      no,      no,      no,   no,   no,    no,    no, },
  /* LAT  */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* LONG */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* FIX  */ { no,      no,      no,      no,   no,   no,    no,    no, },
            }
        }
    },

    {RSHIFT, {">>", rshiftInts, 0,
            {
  // lhs:     |----------------------- rhs ----------------------------|  
  //          INT ,     REAL ,    DEG ,  RAD , GRAD,  LAT ,  LONG , FIX ,  
  /* INT  */ { ok,      no,      no,      no,   no,   no,    no,    no, },
  /* REAL */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* DEG  */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* RAD  */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* GRAD  */{ no,      no,      no,      no,   no,   no,    no,    no, },
  /* LAT  */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* LONG */ { no,      no,      no,      no,   no,   no,    no,    no, },
  /* FIX  */ { no,      no,      no,      no,   no,   no,    no,    no, },
            }
        }
    },

    {'+', {"+", addInts, addReals,
            {
  // lhs:     |----------------------- rhs ----------------------------|  
  //          INT ,     REAL ,    DEG ,   RAD ,    GRAD,    LAT ,    LONG ,   FIX ,  
  /* INT  */ { ok,      castL2R, castL2R, castL2R, castL2R, castL2R, castL2R, no, },
  /* REAL */ { castR2L, ok,      castL2R, castL2R, castL2R, castL2R, castL2R, no, },
  /* DEG  */ { castR2L, castR2L, ok,      no,      castL2R, castL2R, castL2R, no, },
  /* RAD  */ { castR2L, castR2L, no,      ok,      no,      no,      no,      no, },
  /* GRAD  */{ castR2L, castR2L, no,      no,      ok,      no,      no,      no, },
  /* LAT  */ { castR2L, castR2L, castR2L, castR2L, castR2L, no,      no,      no, },
  /* LONG */ { castR2L, castR2L, castR2L, castR2L, castR2L, no,      no,      no, },
  /* FIX  */ { no,      no,      no,      no,      no,      no,      no,      no, },
            }
        }
    },

    {'-', {"-", subInts, subReals,
            {
  // lhs:     |----------------------- rhs ----------------------------|  
  //          INT ,     REAL ,    DEG ,  RAD , GRAD, LAT ,    LONG ,    FIX ,  
  /* INT  */ { ok,      castL2R, no,      no,  no,    no,      no,      no,   },
  /* REAL */ { castR2L, ok,      no,      no,  no,    no,      no,      no,   },
  /* DEG  */ { no,      no,      ok,      no,  no,    no,      no,      no,   },
  /* RAD  */ { no,      no,      no,      ok,  no,    no,      no,      no,   },
  /* GRAD */ { no,      no,      no,      no,  ok,    no,      no,      no,   },
  /* LAT  */ { no,      no,      castR2L, no,  no,    no,      no,      no,   },
  /* LONG */ { no,      no,      castR2L, no,  no,    no,      no,      no,   },
  /* FIX  */ { no,      no,      no,      no,  no,    no,      no,      no,   },
            }
        }
    },

    {'*', {"*", mulInts, mulReals,
            {
  // lhs:     |----------------------- rhs ----------------------------|  
  //          INT ,     REAL ,    DEG ,   RAD ,    GRAD,    LAT ,    LONG ,  FIX ,  
  /* INT  */ { ok,      castL2R, castL2R, castL2R, castL2R, no,      no,      no, },
  /* REAL */ { castR2L, ok,      castL2R, castL2R, castL2R, no,      no,      no, },
  /* DEG  */ { castR2L, castR2L, no,      no,      no,      no,      no,      no, },
  /* RAD  */ { castR2L, castR2L, no,      no,      no,      no,      no,      no, },
  /* GRAD */ { castR2L, castR2L, no,      no,      no,      no,      no,      no, },
  /* LAT  */ { no,      no,      no,      no,      no,      no,      no,      no, },
  /* LONG */ { no,      no,      no,      no,      no,      no,      no,      no, },
  /* FIX  */ { no,      no,      no,      no,      no,      no,      no,      no, },
            }
        }
    },

    {'/', {"/", divInts, divReals,
            {
  // lhs:     |----------------------- rhs ----------------------------| 
  //          INT ,     REAL ,    DEG ,   RAD ,    GRAD,    LAT ,    LONG ,  FIX ,
  /* INT  */ { ok,      castL2R, castL2R, castL2R, castL2R, no,      no,      no, },
  /* REAL */ { castR2L, ok,      castL2R, castL2R, castL2R, no,      no,      no, },
  /* DEG  */ { castR2L, castR2L, no,      no,      no,      no,      no,      no, },
  /* RAD  */ { castR2L, castR2L, no,      no,      no,      no,      no,      no, },
  /* GRAD */ { castR2L, castR2L, no,      no,      no,      no,      no,      no, },
  /* LAT  */ { no,      no,      no,      no,      no,      no,      no,      no, },
  /* LONG */ { no,      no,      no,      no,      no,      no,      no,      no, },
  /* FIX  */ { no,      no,      no,      no,      no,      no,      no,      no, },
            }
        }
    },

    {'%', {"%", modInts, modReals,
            {
  // lhs:     |----------------------- rhs ----------------------------| 
  //          INT ,     REAL ,    DEG ,   RAD ,    GRAD,    LAT ,    LONG ,  FIX ,
  /* INT  */ { ok,      castL2R, castL2R, castL2R, castL2R, no,      no,      no, },
  /* REAL */ { castR2L, ok,      castL2R, castL2R, castL2R, no,      no,      no, },
  /* DEG  */ { castR2L, castR2L, no,      no,      no,      no,      no,      no, },
  /* RAD  */ { castR2L, castR2L, no,      no,      no,      no,      no,      no, },
  /* GRAD */ { castR2L, castR2L, no,      no,      no,      no,      no,      no, },
  /* LAT  */ { no,      no,      no,      no,      no,      no,      no,      no, },
  /* LONG */ { no,      no,      no,      no,      no,      no,      no,      no, },
  /* FIX  */ { no,      no,      no,      no,      no,      no,      no,      no, },
            }
        }
    },
};

