#include "parser.ih"

ArgType Parser::firstArg(ExprData &expr)
{
    return ArgType(rvalue(expr), expr.variables());
}
