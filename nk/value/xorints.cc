#include "value.ih"

long long Value::xorInts(Value const &lhs, Value const &rhs)
{
    return lhs.longint() ^ rhs.longint();
}
