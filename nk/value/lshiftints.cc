#include "value.ih"

long long Value::lshiftInts(Value const &lhs, Value const &rhs)
{
    return lhs.longint() << rhs.longint();
}
