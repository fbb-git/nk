// Generated by Bisonc++ V4.01.01 on Thu, 07 Mar 2013 20:12:06 +0100

#ifndef ParserBase_h_included
#define ParserBase_h_included

#include <vector>
#include <iostream>

// $insert preincludes
#include <memory>
#include <stdexcept>
#include <type_traits>
#include "preincludes.h"

namespace // anonymous
{
    struct PI_;
}


// $insert polymorphic
enum class Tag_
{
    TYPE,
    ARGS,
    EXPR,
};

namespace Meta_
{
    template <Tag_ tag>
    struct TypeOfBase;

    template <typename Tp_>
    struct TagOf;

// $insert polymorphicSpecializations
    template <>
    struct TagOf<TypeData>
    {
        static Tag_ const tag = Tag_::TYPE;
    };

    template <>
    struct TagOf<ArgType>
    {
        static Tag_ const tag = Tag_::ARGS;
    };

    template <>
    struct TagOf<ExprData>
    {
        static Tag_ const tag = Tag_::EXPR;
    };

    template <>
    struct TypeOfBase<Tag_::TYPE>
    {
        typedef TypeData DataType;
    };

    template <>
    struct TypeOfBase<Tag_::ARGS>
    {
        typedef ArgType DataType;
    };

    template <>
    struct TypeOfBase<Tag_::EXPR>
    {
        typedef ExprData DataType;
    };


        // determining the nature of a polymorphic semantic value:
        // if it's a class-type, use 'Type const &' as returntype of const
        // functions; if it's a built-in type (like 'int') use Type:

    struct ClassType
    {
        char _[2];
    };
    
    struct BuiltinType
    {
        char _;
    };

    template <typename T>
    BuiltinType test(...);

    template <typename T>
    ClassType test(void (T::*)());

    template <Tag_ tg_>
    struct TypeOf: public TypeOfBase<tg_>
    {
        typedef typename TypeOfBase<tg_>::DataType DataType;
        enum: bool 
        { 
            isBuiltinType = sizeof(test<DataType>(0)) == sizeof(BuiltinType)
        };

        typedef typename std::conditional<
                    isBuiltinType, DataType, DataType const &
                >::type ReturnType;
    };

        // The Base class: 
        // Individual semantic value classes are derived from this class.
        // This class offers a member returning the value's Tag_
        // and two member templates get() offering const/non-const access to
        // the actual semantic value type.
    class Base
    {
        Tag_ d_tag;
    
        protected:
            Base(Tag_ tag);

        public:
            Base(Base const &other) = delete;
            virtual ~Base();

            Tag_ tag() const;
    
            template <Tag_ tg_>
            typename TypeOf<tg_>::ReturnType get() const;
    
            template <Tag_ tg_>
            typename TypeOf<tg_>::DataType &get();
    };
    
        // The class Semantic is derived from Base. It stores a particular
        // semantic value type. The stored data are declared 'mutable' to
        // allow the definitions of a const and non-const conversion operator.
        // This way, const objects continue to offer non-modifiable data
    template <Tag_ tg_>
    class Semantic: public Base
    {
        typedef typename TypeOf<tg_>::DataType DataType;
    
        mutable DataType d_data;
    
        public:
            typedef typename TypeOf<tg_>::ReturnType ReturnType;
    
                // The default constructor and constructors for 
                // defined data types are available
            Semantic();
            Semantic(DataType const &data);
            Semantic(DataType &&tmp);

                // Conversion operators allow const/non-const access to d_data
            operator ReturnType() const;
            operator DataType &();
    };

        // The class Stype wraps the shared_ptr holding a pointer to Base.
        // It becomes the polymorphic STYPE_
        // Constructors expect (l/r-value) references to defined semantic
        // value types.
        // It also wraps Base's get members, allowing constructions like
        // $$.get<INT> to be used, rather than $$->get<INT>.
        // Furthermore, its operator= can be used to assign a Semantic *
        // directly to the SType object. The free functions (in the parser's
        // namespace (if defined)) semantic_ can be used to obtain a 
        // Semantic *. 
    class SType: public std::shared_ptr<Base>
    {
        public:
            SType() = default;
            SType(SType const &other) = default;
            SType(SType &&tmp) = default;
        
            SType &operator=(SType const &rhs) = default;
            SType &operator=(SType &&tmp) = default;
            template <typename Tp_>
            SType &operator=(Tp_ &&value);

            Tag_ tag() const;

                // this get()-member checks for 0-pointer and correct tag 
                // in shared_ptr<Base>, and may throw a logic_error
            template <Tag_ tg_>                    
            typename TypeOf<tg_>::ReturnType get() const;
    
                // this get()-member checks for 0-pointer and correct tag 
                // in shared_ptr<Base>, and resets the shared_ptr's Base * 
                // to point to Meta::_Semantic<tg_>() if not
            template <Tag_ tg_>
            typename TypeOf<tg_>::DataType &get();

                // the data()-members do not check, and may result in a 
                // bad_cast exception or segfault if used incorrectly

            template <Tag_ tg_>
            typename TypeOf<tg_>::ReturnType data() const;

            template <Tag_ tg_>
            typename TypeOf<tg_>::DataType &data();
    };

}  // namespace Meta_

class ParserBase
{
    public:
// $insert tokens

    // Symbolic tokens:
    enum Tokens_
    {
        PLUS_IS = 257,
        IDENTIFIER,
        TYPE,
        INT,
        RAD,
        REAL,
        ANGLE,
        READ,
        WRITE,
        QUIT,
        HELP,
        LIST,
        RADIX,
        DEG,
        GRAD,
        RADIALS,
        PRE_TOKEN,
        LSHIFT,
        RSHIFT,
    };

// $insert STYPE
    typedef Meta_::SType STYPE_;


    private:
        int d_stackIdx_;
        std::vector<size_t>   d_stateStack_;
        std::vector<STYPE_>  d_valueStack_;

    protected:
        enum Return_
        {
            PARSE_ACCEPT_ = 0,   // values used as parse()'s return values
            PARSE_ABORT_  = 1
        };
        enum ErrorRecovery_
        {
            DEFAULT_RECOVERY_MODE_,
            UNEXPECTED_TOKEN_,
        };
        bool        d_debug_;
        size_t      d_nErrors_;
        size_t      d_requiredTokens_;
        size_t      d_acceptedTokens_;
        int         d_token_;
        int         d_nextToken_;
        size_t      d_state_;
        STYPE_    *d_vsp_;
        STYPE_     d_val_;
        STYPE_     d_nextVal_;

        ParserBase();

        void ABORT() const;
        void ACCEPT() const;
        void ERROR() const;
        void clearin();
        bool debug() const;
        void pop_(size_t count = 1);
        void push_(size_t nextState);
        void popToken_();
        void pushToken_(int token);
        void reduce_(PI_ const &productionInfo);
        void errorVerbose_();
        size_t top_() const;

    public:
        void setDebug(bool mode);
}; 

inline bool ParserBase::debug() const
{
    return d_debug_;
}

inline void ParserBase::setDebug(bool mode)
{
    d_debug_ = mode;
}

inline void ParserBase::ABORT() const
{
    throw PARSE_ABORT_;
}

inline void ParserBase::ACCEPT() const
{
    throw PARSE_ACCEPT_;
}

inline void ParserBase::ERROR() const
{
    throw UNEXPECTED_TOKEN_;
}

// $insert polymorphicInline
namespace Meta_
{

inline Base::Base(Tag_ tag)
:
    d_tag(tag)
{}

inline Tag_ Base::tag() const
{
    return d_tag;
}

template <Tag_ tg_>
inline Semantic<tg_>::Semantic()
:
    Base(tg_),
    d_data(typename TypeOf<tg_>::DataType())
{}

template <Tag_ tg_>
inline Semantic<tg_>::Semantic(typename TypeOf<tg_>::DataType const &data)
:
    Base(tg_),
    d_data(data)
{}

template <Tag_ tg_>
inline Semantic<tg_>::Semantic(typename TypeOf<tg_>::DataType &&tmp)
:
    Base(tg_),
    d_data(std::move(tmp))
{}

template <Tag_ tg_>
inline Semantic<tg_>::operator ReturnType() const
{
    return d_data;
}

template <Tag_ tg_>
inline Semantic<tg_>::operator typename Semantic<tg_>::DataType &()
{
    return d_data;
}

template <Tag_ tg_>
inline typename TypeOf<tg_>::ReturnType Base::get() const
{
    return dynamic_cast<Semantic<tg_> const &>(*this);
}

template <Tag_ tg_>
inline typename TypeOf<tg_>::DataType &Base::get()
{
    return dynamic_cast<Semantic<tg_> &>(*this);
}

inline Tag_ SType::tag() const
{
    return std::shared_ptr<Base>::get()->tag();
}
    
template <Tag_ tg_>
inline typename TypeOf<tg_>::ReturnType SType::get() const
{
    if (std::shared_ptr<Base>::get() == 0  || tag() != tg_)
        throw std::logic_error("undefined semantic value requested");

    return std::shared_ptr<Base>::get()->get<tg_>();
}

template <Tag_ tg_>
inline typename TypeOf<tg_>::DataType &SType::get()
{
                    // if we're not yet holding a (tg_) value, initialize to 
                    // a Semantic<tg_> holding a default value
    if (std::shared_ptr<Base>::get() == 0 || tag() != tg_)
        reset(new Semantic<tg_>());

    return std::shared_ptr<Base>::get()->get<tg_>();
}

template <Tag_ tg_>
inline typename TypeOf<tg_>::ReturnType SType::data() const
{
    return std::shared_ptr<Base>::get()->get<tg_>();
}

template <Tag_ tg_>
inline typename TypeOf<tg_>::DataType &SType::data()
{
    return std::shared_ptr<Base>::get()->get<tg_>();
}

template <bool, typename Tp_>
struct Assign;

template <typename Tp_>
struct Assign<true, Tp_>
{
    static SType &assign(SType *lhs, Tp_ &&tp);
};

template <typename Tp_>
struct Assign<false, Tp_>
{
    static SType &assign(SType *lhs, Tp_ const &tp);
};

template <>
struct Assign<false, SType>
{
    static SType &assign(SType *lhs, SType const &tp);
};

template <typename Tp_>
inline SType &Assign<true, Tp_>::assign(SType *lhs, Tp_ &&tp)
{
    lhs->reset(new Semantic<TagOf<Tp_>::tag>(std::move(tp)));
    return *lhs;
}

template <typename Tp_>
inline SType &Assign<false, Tp_>::assign(SType *lhs, Tp_ const &tp)
{
    lhs->reset(new Semantic<TagOf<Tp_>::tag>(tp));
    return *lhs;
}

inline SType &Assign<false, SType>::assign(SType *lhs, SType const &tp)
{
    return lhs->operator=(tp);
}

template <typename Tp_>
inline SType &SType::operator=(Tp_ &&rhs) 
{
    return Assign<
                std::is_rvalue_reference<Tp_ &&>::value, 
                typename std::remove_reference<Tp_>::type
           >::assign(this, std::forward<Tp_>(rhs));
}

} // namespace Meta_

// As a convenience, when including ParserBase.h its symbols are available as
// symbols in the class Parser, too.
#define Parser ParserBase


#endif


