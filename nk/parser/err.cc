#include "parser.ih"

void Parser::err(std::string const &msg)
{
    cout << msg << '\n';
    ERROR();
}
