#include "parser.ih"

Value Parser::rvalue(ExprData &exprData)
{
    Value ret;

    if (exprData.type() != Value::IDENT)
        ret = exprData.value();
    else
    {
                                // get the data associated with exprData
        Symtab::Data &symData = exprData.symData();



        string line = symData.exprText();       // get the expr. text
        line += ' ';                            // + extra char for searching

        size_t pos = line.find(exprData.ident());   // look for exprData's id.

        char ch = line[pos + exprData.ident().length()];    // ch beyond id.

        bool usesID =                           // use of id itself in the exr
                    pos != string::npos and     // found the id and next char
                    not isalnum(ch) and ch != '_';  // isn't an id-char

//        cerr << '\n' <<
//                    exprData.ident() << ": " << exprData.value() << ", "
//                    "expr text: `" << symData.exprText() << "', "
//                    "nested: " << symData.nested() << ", "
//                    "this ident in expr: " << (pos != string::npos) << ", "
//                    "beyond ident: " << ch << ", uses ID: " << usesID << '\n';
       

                                // when referring to it in a nested expression 
                                // or if there is no evaluation expression
        if (usesID ||   // pos != string::npos || 
                symData.nested() || not symData.hasExprText())
            ret = symData.value();      // then use its current plain value
        else
        {
                            // not nested and there is an evaluation 
                            // expression: store the expression in a stream
            istringstream exprStream(symData.exprText());

                            // now our symbol *is* nested
            symData.pushNested();

            Parser parser(d_symtab, exprStream);

            parser.parse(); // parse the expression

            ret = parser.value();   // get its value
            symData.popNested();    // restore the nested status
        }
    }
    return ret;
}       
