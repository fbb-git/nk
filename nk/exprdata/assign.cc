#include "exprdata.ih"

void ExprData::assign(ExprData const &rhs)
{
    d_end = rhs.d_end;
    d_variables |= rhs.d_variables;
    value() = rhs.value();
}
