#include "value.ih"

bool Value::isLvalue() const
{
    return d_value->type() == IDENT;
}
