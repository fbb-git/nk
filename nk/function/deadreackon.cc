#include "function.ih"

// cosine rule for angles: all angles from the pole in degrees:
//    cos(c) = cos(a) cos(b) + sin(a) sin(b) cos(C).

FixValue::LatLong Function::deadReckon(
                double latitude, double longitude, double headingRad, 
                                    double distanceNM)
{
                                           
    double distance = Value::d2r(distanceNM / 60); // convert distance in NM to
                                            // distance in radians
                                        
    double latFrom = Value::d2r(90 - latitude);    // distance NP to from.lat
                                            // (in radians)

    double latEnd =             // lat.end computed as distance (rad)
        acos(                   // from the pole
                cos(latFrom) * cos(distance) +
                sin(latFrom) * sin(distance) * cos(headingRad)
        );

    double longEnd;              // longitude of end point

    double const r90 = Value::d2r(90);
    double const r180 = Value::d2r(180);
    double const r270 = Value::d2r(270);

                                // end position at the NP or SP: same longit.
    if (Value::isZero(latEnd) || Value::isZero(fabs(latEnd) - r180) ) 
        longEnd = 0;
    else
    { 
        longEnd = asin( sin(headingRad) * sin(distance) / sin(latEnd) );

        if (distance > r90 && distance < r270)  // distance in Q2 or Q3:
            longEnd = r180 - longEnd;           // longit. wrt 180 deg.
    }
                                            // dist. from the begin longit.
    longEnd += Value::d2r(longitude);
    latEnd = r90 - latEnd;                  // distance from the equator.


    return FixValue::LatLong {Value::r2d(latEnd), Value::r2d(longEnd)};
}



