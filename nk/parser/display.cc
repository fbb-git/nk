#include "parser.ih"

void Parser::display(ExprData &exprData)
{
    if (not d_show || d_token != '\n')
    {
        d_show = true;
        return;
    }

    Value ret = rvalue(exprData);

    // cout << "display 2: " << exprData.value() << ", ret = " << ret << '\n';

    if (not (ret.type() == Value::INT && d_displayRadix != 10))
        cout << ret << '\n';
    else
    {
        displayInt(ret.longint());
        cout << '\n';
    }
//    cout << "display 3: " << exprData.value() << '\n';
}
