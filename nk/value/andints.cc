#include "value.ih"

long long Value::andInts(Value const &lhs, Value const &rhs)
{
    return lhs.longint() & rhs.longint();
}
