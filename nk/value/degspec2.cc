#include "value.ih"

double Value::degSpec2(string const &matched)
{
    istringstream in(matched);

    size_t part1;
    in >> part1;
    in.ignore();

    size_t part2;
    in >> part2;
    in.ignore();

    return part1 + part2 / 60.0;
}

