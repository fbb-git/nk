#include "valuebase.ih"

void ValueBase::vNegate()
{
    throw Exception() << "negate not defined for " << typeName(d_type);
}
