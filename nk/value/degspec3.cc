#include "value.ih"

double Value::degSpec3(string const &matched)
{
    istringstream in(matched);

    size_t part1;
    in >> part1;
    in.ignore();

    size_t part2;
    in >> part2;
    in.ignore();

    size_t part3;
    in >> part3;
    return part1 + part2 / 60.0 + part3 / 3600.0;
}

