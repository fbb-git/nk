#include "parser.ih"

void Parser::help() const
{
    cout << 
    "  Data types: int, real, rad (r suffix), deg (d, :), grad (g)\n"
    "               lat (deg [ns], long (deg [ew]), fix\n"
    "  r\\value: value expressed in radix r (2 <= r <= 36)\n"
    "  Variables: supported.  Fixes: .lat, .long yield latitude/longitude\n"
    "  Functions: (a)sin, (a)cos, (a)tan, log, exp, pow, round, sqrt, "
                                                                "trunc,\n"
    "             dist, dr, fx, hdg\n"
    "  Operators: unary: -, (), (type);  binary:  * / %  << >> + -   =\n"
    "  Commands: ^/cmd, unique head OK.\n"
    "       /quit, /help, /list, /rad, /deg, /grad, /fix [ndecimals] (omit: "
                                                            "off)\n"
    "       /read [+=] [fname], /write [fname], /# (display radix "
                                                            "(2 <= # <= 36)\n"
    "\n"
    "  Start the program with the --help option to see its command-line "
                                                                "options\n"
    ;
}
