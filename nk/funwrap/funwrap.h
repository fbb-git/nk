#ifndef INCLUDED_FUNWRAP_
#define INCLUDED_FUNWRAP_

#include <memory>
#include "../funbase/funbase.h"

class Value;

class FunWrap
{
    std::shared_ptr<FunBase> d_base;

    public:
        FunWrap(FunBase *funBase = 0);

        Value operator()(size_t nArgs, Value *args) const;
};

#endif
