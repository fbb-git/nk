#include "parser.ih"

Parser::Parser(Symtab &symtab)
:
    d_symtab(symtab),
    d_scanner(s_in),
    d_data(d_symtab),
    d_msg("syntax error"),
    d_begin(0),
    d_end(0),
    d_backChars("dgrnesw ")      // must match s_vType[] (+ spare char)
{}
