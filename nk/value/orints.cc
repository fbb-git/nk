#include "value.ih"

long long Value::orInts(Value const &lhs, Value const &rhs)
{
    return lhs.longint() | rhs.longint();
}
