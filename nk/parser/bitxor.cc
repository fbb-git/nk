#include "parser.ih"

// support functions, missing in <functional>
Value Parser::bitXor(Value const &left, Value const &right)
{
    return left ^ right;
}

