#include "data.ih"

//  firstValue is a plain value or it is the first value of 
//  a dd:mm:ss specification

Symtab::Data Data::getValue(istringstream &in, 
                     Value::Type type, double firstValue) const
{
    Symtab::Data ret;

    switch (type)
    {
        case ValueTypes::LAT:        // value stored as degrees
        case ValueTypes::LONG:       // value stored as degrees
        case ValueTypes::DEG:
            firstValue = toCoord(in, firstValue, "NS");
        // FALLING THROUGH
        case ValueTypes::INT:
        case ValueTypes::REAL:
        case ValueTypes::RAD:
        case ValueTypes::GRAD:
            ret.set(Value::factory(type, firstValue));
        break;

        case ValueTypes::FIX:
        {
            firstValue = toCoord(in, firstValue, "NS");
            double secondValue;
            in >> secondValue;
            secondValue = toCoord(in, secondValue, "EW");
            ret.set(Value::factory(firstValue, secondValue));
        }
        break;
            
        case ValueTypes::IDENT:     // does not occur
        break;
    }        



//    if (type != Value::FIX)
//        ret.set(Value::factory(type, firstValue));
//    else
//    {
//        double longitude;
//        if (not (in >> longitude))     // err if longitude is missing
//            throw Exception() << "longitude of FIX value missing";
//
//        ret.set(Value::factory(firstValue, longitude));
//    }
//
//    string exprText;
//    if (getline(in, exprText))
//        ret.set(exprText);

    return ret;
}
