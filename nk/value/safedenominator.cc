#include "value.ih"

void Value::safeDenominator(char operation, double value)
{
    if (fabs(value) < s_minimum)
        throw Exception() << "can't compute x " << operation << " 0";
}
