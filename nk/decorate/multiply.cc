#include "parser.ih"

ExprData Parser::multiply(ExprData &left, ExprData &right)
{
    ExprData ret(rvalue(left) * rvalue(right), 
                left.begin(), right.end(), 
                left.variables() || right.variables());

    return ret;
}
