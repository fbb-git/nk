#include "valuebase.ih"

bool operator==(ValueBase const &lhs, ValueBase const &rhs)
{
    return lhs.equals(rhs);
}
