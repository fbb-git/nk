#include "parser.ih"

void Parser::addArg(ArgType &args, ExprData &expr)
{
    args.arg.push_back(rvalue(expr));
    args.variables  |= expr.variables();
}
