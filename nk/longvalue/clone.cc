#include "longvalue.ih"

ValueBase *LongValue::clone() const
{
    return new LongValue(value());
}
