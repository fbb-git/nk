#include "exprdata.ih"

void ExprData::append(ExprData const &rhs)
{
    d_end = rhs.d_end;
    d_variables |= rhs.d_variables;
}
