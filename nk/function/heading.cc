#include "function.ih"

/*
    Using 

            Triangle

                gamma
                 /  \
                /    \
               /      \ a
              /        \
             /          \
      alpha  ------------  
      FROM        c       TO

        Where: gamma: deltaLongitude
                   c: distance FROM-TO
                   a: complement TO.latitude

        Sine rule: sin(gamma) / sin(c) = sin(alpha) / sin(a)

                   alpha = asin(sin(gamma) / sin(c) * sin(a))

    Since sin(a) == sin(180 - a) == -sin(360 - a) == -sin(180 + a) and 
    since we're using absolute deltaLongitudes, so -c isn't used, the computed
    angle is always in quadrant 1 (Q1). So we have to correct for the
    quadrant of the destination wrt the origin.

    deltas are computed as: dest.coordinate - src.coordinate

                            |
                            |
                dlongitude  |
                neg         |
                            |
               -------------+---------------
                            |
                dlatitude   |   dlatitude 
                neg         |   neg
                            |
                dlongitude  |
                neg         |
                            |

  ---------------------------------------------------------------------------
  deltaLongitude. < 0        deltaLatitude < 0        Quadrant    alpha =
  ---------------------------------------------------------------------------
       true                         true                  4       180 + alpha
       true                         false                 3       360 - alpha
       false                        true                  2       180 - alpha
       false                        false                 1       alpha
  ---------------------------------------------------------------------------

    Zero distances or global opposites (on opposite meridians, 180 deg. apart)
    are handled handled separately

*/

double Function::heading(Value const &from, Value const &to) 
{
    double dist = distance(from, to) / 60;  // distance in degrees

    if (Value::isZero(fmod(dist, 180))) // distance == 0 or to/from on
        return 0;                       // opposite meridians then arbitrarily 
                                        // go north

                                        // Angle from pole to to-latitude
    double toAngle = 90 - to.latitude(); 
    
    double deltaLong = Value::d2r(to.longitude() - from.longitude());

    double ret =                        // assume Q1
        Value::r2d(
            asin(                 
                sin(fabs(deltaLong)) * sin(Value::d2r(toAngle)) 
                / 
                sin(Value::d2r(dist))
            )
        );

    double deltaLat = to.latitude() - from.latitude();

    if (deltaLong < 0)
    {
        if (deltaLat < 0)
            ret += 180;                     // Q4
        else
            ret = 360 - ret;                // Q3
    }
    else if (deltaLat < 0)                  // Q2
        ret = 180 - ret;

    return ret;
}






