#include "parser.ih"

void Parser::assign(ExprData &lhs, ExprData const &rhs)
{
    if (lhs.type() != Value::IDENT)
        err("lvalue must be variable");

//    cout << "ASSIGN: lhs value = " << lhs.value() << 
//                ", rhs value = " <<
//            rhs.value() << ", variables = " << rhs.variables()  << ": " << 
//            d_line.substr(rhs.begin(), rhs.length()) << endl;

    lhs.assign(rhs);

//    cout << "NEW VALUE: lhs value = " << lhs.value() << endl;

    lhs.symData().set(rhs.variables() ?
                            d_line.substr(rhs.begin(), rhs.length())
                        :
                            ""
                    );
//
//    cout << "ASSIGN OUT: lhs value = " << lhs.value() << 
//                ", rhs value = " <<
//            rhs.value() << ", variables = " << rhs.variables()  << ": " << 
//            d_line.substr(rhs.begin(), rhs.length()) << endl;
}




