#include "fixvalue.ih"

FixValue::FixValue(double latitude, double longitude)
:
    ValueBase(FIX),
    d_latitude(latitude),
    d_longitude(longitude)
{}
