#include "scanner.ih"

int Scanner::command()
{
    string const &str = matched();

    auto const &iter = find_if(s_commands, s_commandsEnd,
        [&](CommandType const &cmdType)
        {
            return str.length() >= cmdType.minCmdLength &&
                   cmdType.cmd.find(str) == 0;
        }
    );

    if (iter == s_commandsEnd)
    {
        if (isdigit(str[1]))
            return Parser::RADIX;

        throw Exception() << "Command `" << str << "' not implemented\n";
    }

    return iter->token;
}



