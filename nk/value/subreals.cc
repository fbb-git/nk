#include "value.ih"

double Value::subReals(Value const &lhs, Value const &rhs)
{
    return lhs.real() - rhs.real();
}
