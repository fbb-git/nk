#include "data.ih"

Data::~Data()
{
    string name;
    if (Arg::instance().option(&name, 'w'))
    {
        if (name.empty())
            name = defaultName();
        write(name);
    }
}
