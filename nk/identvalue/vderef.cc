#include "identvalue.ih"

Value *IdentValue::vDeref() const
{
    return &d_ident->value();
}
