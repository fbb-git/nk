#include "radvalue.ih"

bool RadValue::equals(ValueBase const &rhs) const
{
    return rhs.type() == RAD && value() == rhs.real();
}
