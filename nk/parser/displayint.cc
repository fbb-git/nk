#include "parser.ih"

void Parser::displayInt(long long value) const
{
    if (value < 0)
    {
        cout << '-';
        value = -value;
    }

    stack<int> digits;
    do
        digits.push(value % d_displayRadix);
    while (value /= d_displayRadix);

    while (digits.size())
    {
        cout << Value::numberChar(digits.top());
        digits.pop();
    }
}
