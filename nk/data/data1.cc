#include "data.ih"

Data::Data(Symtab &symtab)
:
    d_symtab(symtab)
{
    string name;
    if (Arg::instance().option(&name, 'r'))
    {
        if (name.empty())
            name = defaultName();
        if (access(name.c_str(), F_OK) == 0)
            read(name);
        else
            cout << "[Warning] no file `" << name << "'\n";
    }
}
