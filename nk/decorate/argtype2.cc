#include "preincludes.h"

ArgType::ArgType(Value const &value, bool vars)
:
    arg(1, value),
    variables(vars)
{}
