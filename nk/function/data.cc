#include "function.ih"

// see expr/functioncall.cc for details

Function::FunMap const Function::s_function =
{
//                                               types:
//                                               (out_) arg(s)
    {"acos",   makeHandler<RadValue>("acos",  1, real,          ::acos)},
    {"asin",   makeHandler<RadValue>("asin",  1, real,          ::asin)},
    {"atan",   makeHandler<RadValue>("atan",  1, real,          ::atan)},
    {"cos",    makeHandler<RealValue>("cos",  1, angle_real,    ::cos)},
    {"dist",   makeHandler<RealValue>("dist", 2, fix2,          distance)},
    {"exp",    makeHandler<RealValue>("exp",  1, real,          ::exp)},
    {"fx",     makeHandler<FixValue> ("fx",   2, latLong,       0)},
    {"hdg",    makeHandler<DegValue> ("hdg",  2, fix2,          heading)},
    {"log",    makeHandler<RealValue>("log",  1, real,          ::log)},
    {"pow",    makeHandler<RealValue>("pow",  2, real2,         ::pow)},
    {"round",  makeHandler<RealValue>("round",1, real,          ::round)},
    {"sin",    makeHandler<RealValue>("sin",  1, angle_real,    ::sin)},
    {"sqrt",   makeHandler<RealValue>("sqrt", 1, real,          ::sqrt)},
    {"tan",    makeHandler<RealValue>("tan",  1, angle_real,    ::tan)},
    {"trunc",  makeHandler<RealValue>("trunc",1, real,          ::trunc)},
    {"dr",     makeHandler<FixValue> ("dr",   3, fixDegReal,    deadReckon)},
};







