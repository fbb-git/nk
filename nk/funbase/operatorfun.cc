#include "funbase.ih"

// see expr/functioncall.cc for details

Value FunBase::operator()(size_t nArgs, Value *args) const
{
    Value ret;

    if (d_arity != nArgs)
        throw Exception() << "function " << d_name << " requires " << 
                            d_arity << " arguments, not " << nArgs;

    ret = call(args);
    return ret;
}
