// Generated by Bisonc++ V4.01.01 on Thu, 07 Mar 2013 20:12:06 +0100

// $insert class.ih
#include "parser.ih"

// The FIRST element of SR arrays shown below uses `d_type', defining the
// state's type, and `d_lastIdx' containing the last element's index. If
// d_lastIdx contains the REQ_TOKEN bitflag (see below) then the state needs
// a token: if in this state d_token_ is _UNDETERMINED_, nextToken() will be
// called

// The LAST element of SR arrays uses `d_token' containing the last retrieved
// token to speed up the (linear) seach.  Except for the first element of SR
// arrays, the field `d_action' is used to determine what to do next. If
// positive, it represents the next state (used with SHIFT); if zero, it
// indicates `ACCEPT', if negative, -d_action represents the number of the
// rule to reduce to.

// `lookup()' tries to find d_token_ in the current SR array. If it fails, and
// there is no default reduction UNEXPECTED_TOKEN_ is thrown, which is then
// caught by the error-recovery function.

// The error-recovery function will pop elements off the stack until a state
// having bit flag ERR_ITEM is found. This state has a transition on _error_
// which is applied. In this _error_ state, while the current token is not a
// proper continuation, new tokens are obtained by nextToken(). If such a
// token is found, error recovery is successful and the token is
// handled according to the error state's SR table and parsing continues.
// During error recovery semantic actions are ignored.

// A state flagged with the DEF_RED flag will perform a default
// reduction if no other continuations are available for the current token.

// The ACCEPT STATE never shows a default reduction: when it is reached the
// parser returns ACCEPT(). During the grammar
// analysis phase a default reduction may have been defined, but it is
// removed during the state-definition phase.

// So:
//      s_x[] = 
//      {
//                  [_field_1_]         [_field_2_]
//
// First element:   {state-type,        idx of last element},
// Other elements:  {required token,    action to perform},
//                                      ( < 0: reduce, 
//                                          0: ACCEPT,
//                                        > 0: next state)
// Last element:    {set to d_token_,    action to perform}
//      }

// When the --thread-safe option is specified, all static data are defined as
// const. If --thread-safe is not provided, the state-tables are not defined
// as const, since the lookup() function below will modify them


namespace // anonymous
{
    char const author[] = "Frank B. Brokken (f.b.brokken@rug.nl)";

    enum 
    {
        STACK_EXPANSION = 5     // size to expand the state-stack with when
                                // full
    };

    enum ReservedTokens
    {
        PARSE_ACCEPT     = 0,   // `ACCEPT' TRANSITION
        _UNDETERMINED_   = -2,
        _EOF_            = -1,
        _error_          = 256
    };
    enum StateType       // modify statetype/data.cc when this enum changes
    {
        NORMAL,
        ERR_ITEM,
        REQ_TOKEN,
        ERR_REQ,    // ERR_ITEM | REQ_TOKEN
        DEF_RED,    // state having default reduction
        ERR_DEF,    // ERR_ITEM | DEF_RED
        REQ_DEF,    // REQ_TOKEN | DEF_RED
        ERR_REQ_DEF // ERR_ITEM | REQ_TOKEN | DEF_RED
    };    
    struct PI_     // Production Info
    {
        size_t d_nonTerm; // identification number of this production's
                            // non-terminal 
        size_t d_size;    // number of elements in this production 
    };

    struct SR_     // Shift Reduce info, see its description above
    {
        union
        {
            int _field_1_;      // initializer, allowing initializations 
                                // of the SR s_[] arrays
            int d_type;
            int d_token;
        };
        union
        {
            int _field_2_;

            int d_lastIdx;          // if negative, the state uses SHIFT
            int d_action;           // may be negative (reduce), 
                                    // postive (shift), or 0 (accept)
            size_t d_errorState;    // used with Error states
        };
    };

    // $insert staticdata
    
// Productions Info Records:
PI_ const s_productionInfo[] = 
{
     {0, 0}, // not used: reduction values are negative
     {280, 1}, // 1: startrule ->  lines
     {280, 3}, // 2: startrule (PRE_TOKEN) ->  PRE_TOKEN expr '\x0a'
     {281, 2}, // 3: lines ->  lines line
     {281, 0}, // 4: lines ->  <empty>
     {282, 2}, // 5: line ('\x0a') ->  input '\x0a'
     {283, 1}, // 6: input ->  command
     {283, 1}, // 7: input (_error_) ->  _error_
     {283, 1}, // 8: input ->  expression
     {283, 0}, // 9: input ->  <empty>
     {285, 1}, // 10: expression ->  expr
     {286, 1}, // 11: _opt_ident (IDENTIFIER) ->  IDENTIFIER
     {286, 0}, // 12: _opt_ident ->  <empty>
     {284, 2}, // 13: command (READ) ->  READ _opt_ident
     {284, 3}, // 14: command (READ) ->  READ PLUS_IS _opt_ident
     {284, 1}, // 15: command (WRITE) ->  WRITE
     {284, 1}, // 16: command (QUIT) ->  QUIT
     {284, 1}, // 17: command (HELP) ->  HELP
     {284, 1}, // 18: command (LIST) ->  LIST
     {284, 1}, // 19: command (RADIX) ->  RADIX
     {284, 1}, // 20: command (DEG) ->  DEG
     {284, 1}, // 21: command (GRAD) ->  GRAD
     {284, 1}, // 22: command (RADIALS) ->  RADIALS
     {287, 1}, // 23: _type (TYPE) ->  TYPE
     {288, 3}, // 24: _args (',') ->  _args ',' expr
     {288, 1}, // 25: _args ->  expr
     {289, 1}, // 26: _optArgs ->  _args
     {289, 0}, // 27: _optArgs ->  <empty>
     {290, 1}, // 28: expr (INT) ->  INT
     {290, 1}, // 29: expr (RAD) ->  RAD
     {290, 1}, // 30: expr (REAL) ->  REAL
     {290, 1}, // 31: expr (ANGLE) ->  ANGLE
     {290, 1}, // 32: expr (IDENTIFIER) ->  IDENTIFIER
     {290, 3}, // 33: expr ('=') ->  expr '=' expr
     {290, 3}, // 34: expr ('&') ->  expr '&' expr
     {290, 3}, // 35: expr ('^') ->  expr '^' expr
     {290, 3}, // 36: expr ('|') ->  expr '|' expr
     {290, 3}, // 37: expr ('+') ->  expr '+' expr
     {290, 3}, // 38: expr ('-') ->  expr '-' expr
     {290, 3}, // 39: expr (LSHIFT) ->  expr LSHIFT expr
     {290, 3}, // 40: expr (RSHIFT) ->  expr RSHIFT expr
     {290, 3}, // 41: expr ('*') ->  expr '*' expr
     {290, 3}, // 42: expr ('/') ->  expr '/' expr
     {290, 3}, // 43: expr ('%') ->  expr '%' expr
     {290, 2}, // 44: expr ('(') ->  '-' expr
     {290, 4}, // 45: expr ('(') ->  '(' _type ')' expr
     {290, 3}, // 46: expr ('(') ->  '(' expr ')'
     {290, 4}, // 47: expr ('(') ->  expr '(' _optArgs ')'
     {291, 1}, // 48: startrule_$ ->  startrule
};

// State info and SR_ transitions for each state.


SR_ s_0[] =
{
    { { REQ_DEF}, {  4} },             
    { {     280}, {  1} }, // startrule
    { {     281}, {  2} }, // lines    
    { {     273}, {  3} }, // PRE_TOKEN
    { {       0}, { -4} },             
};

SR_ s_1[] =
{
    { { REQ_TOKEN}, {            2} }, 
    { {     _EOF_}, { PARSE_ACCEPT} }, 
    { {         0}, {            0} }, 
};

SR_ s_2[] =
{
    { { ERR_REQ_DEF}, { 24} },              
    { {         282}, {  4} }, // line      
    { {         283}, {  5} }, // input     
    { {         284}, {  6} }, // command   
    { {     _error_}, {  7} }, // _error_   
    { {         285}, {  8} }, // expression
    { {         264}, {  9} }, // READ      
    { {         265}, { 10} }, // WRITE     
    { {         266}, { 11} }, // QUIT      
    { {         267}, { 12} }, // HELP      
    { {         268}, { 13} }, // LIST      
    { {         269}, { 14} }, // RADIX     
    { {         270}, { 15} }, // DEG       
    { {         271}, { 16} }, // GRAD      
    { {         272}, { 17} }, // RADIALS   
    { {         290}, { 18} }, // expr      
    { {         260}, { 19} }, // INT       
    { {         261}, { 20} }, // RAD       
    { {         262}, { 21} }, // REAL      
    { {         263}, { 22} }, // ANGLE     
    { {         258}, { 23} }, // IDENTIFIER
    { {          45}, { 24} }, // '-'       
    { {          40}, { 25} }, // '('       
    { {       _EOF_}, { -1} }, // _EOF_     
    { {           0}, { -9} },              
};

SR_ s_3[] =
{
    { { REQ_TOKEN}, {  9} },              
    { {       290}, { 26} }, // expr      
    { {       260}, { 19} }, // INT       
    { {       261}, { 20} }, // RAD       
    { {       262}, { 21} }, // REAL      
    { {       263}, { 22} }, // ANGLE     
    { {       258}, { 23} }, // IDENTIFIER
    { {        45}, { 24} }, // '-'       
    { {        40}, { 25} }, // '('       
    { {         0}, {  0} },              
};

SR_ s_4[] =
{
    { { DEF_RED}, {  1} }, 
    { {       0}, { -3} }, 
};

SR_ s_5[] =
{
    { { REQ_TOKEN}, {  2} },          
    { {        10}, { 27} }, // '\x0a'
    { {         0}, {  0} },          
};

SR_ s_6[] =
{
    { { DEF_RED}, {  1} }, 
    { {       0}, { -6} }, 
};

SR_ s_7[] =
{
    { { DEF_RED}, {  1} }, 
    { {       0}, { -7} }, 
};

SR_ s_8[] =
{
    { { DEF_RED}, {  1} }, 
    { {       0}, { -8} }, 
};

SR_ s_9[] =
{
    { { REQ_DEF}, {   4} },              
    { {     286}, {  28} }, // _opt_ident
    { {     257}, {  29} }, // PLUS_IS   
    { {     258}, {  30} }, // IDENTIFIER
    { {       0}, { -12} },              
};

SR_ s_10[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -15} }, 
};

SR_ s_11[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -16} }, 
};

SR_ s_12[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -17} }, 
};

SR_ s_13[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -18} }, 
};

SR_ s_14[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -19} }, 
};

SR_ s_15[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -20} }, 
};

SR_ s_16[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -21} }, 
};

SR_ s_17[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -22} }, 
};

SR_ s_18[] =
{
    { { REQ_DEF}, {  13} },          
    { {      61}, {  31} }, // '='   
    { {      38}, {  32} }, // '&'   
    { {      94}, {  33} }, // '^'   
    { {     124}, {  34} }, // '|'   
    { {      43}, {  35} }, // '+'   
    { {      45}, {  36} }, // '-'   
    { {     274}, {  37} }, // LSHIFT
    { {     275}, {  38} }, // RSHIFT
    { {      42}, {  39} }, // '*'   
    { {      47}, {  40} }, // '/'   
    { {      37}, {  41} }, // '%'   
    { {      40}, {  42} }, // '('   
    { {       0}, { -10} },          
};

SR_ s_19[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -28} }, 
};

SR_ s_20[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -29} }, 
};

SR_ s_21[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -30} }, 
};

SR_ s_22[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -31} }, 
};

SR_ s_23[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -32} }, 
};

SR_ s_24[] =
{
    { { REQ_TOKEN}, {  9} },              
    { {       290}, { 43} }, // expr      
    { {       260}, { 19} }, // INT       
    { {       261}, { 20} }, // RAD       
    { {       262}, { 21} }, // REAL      
    { {       263}, { 22} }, // ANGLE     
    { {       258}, { 23} }, // IDENTIFIER
    { {        45}, { 24} }, // '-'       
    { {        40}, { 25} }, // '('       
    { {         0}, {  0} },              
};

SR_ s_25[] =
{
    { { REQ_TOKEN}, { 11} },              
    { {       287}, { 44} }, // _type     
    { {       290}, { 45} }, // expr      
    { {       259}, { 46} }, // TYPE      
    { {       260}, { 19} }, // INT       
    { {       261}, { 20} }, // RAD       
    { {       262}, { 21} }, // REAL      
    { {       263}, { 22} }, // ANGLE     
    { {       258}, { 23} }, // IDENTIFIER
    { {        45}, { 24} }, // '-'       
    { {        40}, { 25} }, // '('       
    { {         0}, {  0} },              
};

SR_ s_26[] =
{
    { { REQ_TOKEN}, { 14} },          
    { {        10}, { 47} }, // '\x0a'
    { {        61}, { 31} }, // '='   
    { {        38}, { 32} }, // '&'   
    { {        94}, { 33} }, // '^'   
    { {       124}, { 34} }, // '|'   
    { {        43}, { 35} }, // '+'   
    { {        45}, { 36} }, // '-'   
    { {       274}, { 37} }, // LSHIFT
    { {       275}, { 38} }, // RSHIFT
    { {        42}, { 39} }, // '*'   
    { {        47}, { 40} }, // '/'   
    { {        37}, { 41} }, // '%'   
    { {        40}, { 42} }, // '('   
    { {         0}, {  0} },          
};

SR_ s_27[] =
{
    { { DEF_RED}, {  1} }, 
    { {       0}, { -5} }, 
};

SR_ s_28[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -13} }, 
};

SR_ s_29[] =
{
    { { REQ_DEF}, {   3} },              
    { {     286}, {  48} }, // _opt_ident
    { {     258}, {  30} }, // IDENTIFIER
    { {       0}, { -12} },              
};

SR_ s_30[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -11} }, 
};

SR_ s_31[] =
{
    { { REQ_TOKEN}, {  9} },              
    { {       290}, { 49} }, // expr      
    { {       260}, { 19} }, // INT       
    { {       261}, { 20} }, // RAD       
    { {       262}, { 21} }, // REAL      
    { {       263}, { 22} }, // ANGLE     
    { {       258}, { 23} }, // IDENTIFIER
    { {        45}, { 24} }, // '-'       
    { {        40}, { 25} }, // '('       
    { {         0}, {  0} },              
};

SR_ s_32[] =
{
    { { REQ_TOKEN}, {  9} },              
    { {       290}, { 50} }, // expr      
    { {       260}, { 19} }, // INT       
    { {       261}, { 20} }, // RAD       
    { {       262}, { 21} }, // REAL      
    { {       263}, { 22} }, // ANGLE     
    { {       258}, { 23} }, // IDENTIFIER
    { {        45}, { 24} }, // '-'       
    { {        40}, { 25} }, // '('       
    { {         0}, {  0} },              
};

SR_ s_33[] =
{
    { { REQ_TOKEN}, {  9} },              
    { {       290}, { 51} }, // expr      
    { {       260}, { 19} }, // INT       
    { {       261}, { 20} }, // RAD       
    { {       262}, { 21} }, // REAL      
    { {       263}, { 22} }, // ANGLE     
    { {       258}, { 23} }, // IDENTIFIER
    { {        45}, { 24} }, // '-'       
    { {        40}, { 25} }, // '('       
    { {         0}, {  0} },              
};

SR_ s_34[] =
{
    { { REQ_TOKEN}, {  9} },              
    { {       290}, { 52} }, // expr      
    { {       260}, { 19} }, // INT       
    { {       261}, { 20} }, // RAD       
    { {       262}, { 21} }, // REAL      
    { {       263}, { 22} }, // ANGLE     
    { {       258}, { 23} }, // IDENTIFIER
    { {        45}, { 24} }, // '-'       
    { {        40}, { 25} }, // '('       
    { {         0}, {  0} },              
};

SR_ s_35[] =
{
    { { REQ_TOKEN}, {  9} },              
    { {       290}, { 53} }, // expr      
    { {       260}, { 19} }, // INT       
    { {       261}, { 20} }, // RAD       
    { {       262}, { 21} }, // REAL      
    { {       263}, { 22} }, // ANGLE     
    { {       258}, { 23} }, // IDENTIFIER
    { {        45}, { 24} }, // '-'       
    { {        40}, { 25} }, // '('       
    { {         0}, {  0} },              
};

SR_ s_36[] =
{
    { { REQ_TOKEN}, {  9} },              
    { {       290}, { 54} }, // expr      
    { {       260}, { 19} }, // INT       
    { {       261}, { 20} }, // RAD       
    { {       262}, { 21} }, // REAL      
    { {       263}, { 22} }, // ANGLE     
    { {       258}, { 23} }, // IDENTIFIER
    { {        45}, { 24} }, // '-'       
    { {        40}, { 25} }, // '('       
    { {         0}, {  0} },              
};

SR_ s_37[] =
{
    { { REQ_TOKEN}, {  9} },              
    { {       290}, { 55} }, // expr      
    { {       260}, { 19} }, // INT       
    { {       261}, { 20} }, // RAD       
    { {       262}, { 21} }, // REAL      
    { {       263}, { 22} }, // ANGLE     
    { {       258}, { 23} }, // IDENTIFIER
    { {        45}, { 24} }, // '-'       
    { {        40}, { 25} }, // '('       
    { {         0}, {  0} },              
};

SR_ s_38[] =
{
    { { REQ_TOKEN}, {  9} },              
    { {       290}, { 56} }, // expr      
    { {       260}, { 19} }, // INT       
    { {       261}, { 20} }, // RAD       
    { {       262}, { 21} }, // REAL      
    { {       263}, { 22} }, // ANGLE     
    { {       258}, { 23} }, // IDENTIFIER
    { {        45}, { 24} }, // '-'       
    { {        40}, { 25} }, // '('       
    { {         0}, {  0} },              
};

SR_ s_39[] =
{
    { { REQ_TOKEN}, {  9} },              
    { {       290}, { 57} }, // expr      
    { {       260}, { 19} }, // INT       
    { {       261}, { 20} }, // RAD       
    { {       262}, { 21} }, // REAL      
    { {       263}, { 22} }, // ANGLE     
    { {       258}, { 23} }, // IDENTIFIER
    { {        45}, { 24} }, // '-'       
    { {        40}, { 25} }, // '('       
    { {         0}, {  0} },              
};

SR_ s_40[] =
{
    { { REQ_TOKEN}, {  9} },              
    { {       290}, { 58} }, // expr      
    { {       260}, { 19} }, // INT       
    { {       261}, { 20} }, // RAD       
    { {       262}, { 21} }, // REAL      
    { {       263}, { 22} }, // ANGLE     
    { {       258}, { 23} }, // IDENTIFIER
    { {        45}, { 24} }, // '-'       
    { {        40}, { 25} }, // '('       
    { {         0}, {  0} },              
};

SR_ s_41[] =
{
    { { REQ_TOKEN}, {  9} },              
    { {       290}, { 59} }, // expr      
    { {       260}, { 19} }, // INT       
    { {       261}, { 20} }, // RAD       
    { {       262}, { 21} }, // REAL      
    { {       263}, { 22} }, // ANGLE     
    { {       258}, { 23} }, // IDENTIFIER
    { {        45}, { 24} }, // '-'       
    { {        40}, { 25} }, // '('       
    { {         0}, {  0} },              
};

SR_ s_42[] =
{
    { { REQ_DEF}, {  11} },              
    { {     289}, {  60} }, // _optArgs  
    { {     288}, {  61} }, // _args     
    { {     290}, {  62} }, // expr      
    { {     260}, {  19} }, // INT       
    { {     261}, {  20} }, // RAD       
    { {     262}, {  21} }, // REAL      
    { {     263}, {  22} }, // ANGLE     
    { {     258}, {  23} }, // IDENTIFIER
    { {      45}, {  24} }, // '-'       
    { {      40}, {  25} }, // '('       
    { {       0}, { -27} },              
};

SR_ s_43[] =
{
    { { REQ_DEF}, {   2} },       
    { {      40}, {  42} }, // '('
    { {       0}, { -44} },       
};

SR_ s_44[] =
{
    { { REQ_TOKEN}, {  2} },       
    { {        41}, { 63} }, // ')'
    { {         0}, {  0} },       
};

SR_ s_45[] =
{
    { { REQ_TOKEN}, { 14} },          
    { {        41}, { 64} }, // ')'   
    { {        61}, { 31} }, // '='   
    { {        38}, { 32} }, // '&'   
    { {        94}, { 33} }, // '^'   
    { {       124}, { 34} }, // '|'   
    { {        43}, { 35} }, // '+'   
    { {        45}, { 36} }, // '-'   
    { {       274}, { 37} }, // LSHIFT
    { {       275}, { 38} }, // RSHIFT
    { {        42}, { 39} }, // '*'   
    { {        47}, { 40} }, // '/'   
    { {        37}, { 41} }, // '%'   
    { {        40}, { 42} }, // '('   
    { {         0}, {  0} },          
};

SR_ s_46[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -23} }, 
};

SR_ s_47[] =
{
    { { DEF_RED}, {  1} }, 
    { {       0}, { -2} }, 
};

SR_ s_48[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -14} }, 
};

SR_ s_49[] =
{
    { { REQ_DEF}, {  13} },          
    { {      61}, {  31} }, // '='   
    { {      38}, {  32} }, // '&'   
    { {      94}, {  33} }, // '^'   
    { {     124}, {  34} }, // '|'   
    { {      43}, {  35} }, // '+'   
    { {      45}, {  36} }, // '-'   
    { {     274}, {  37} }, // LSHIFT
    { {     275}, {  38} }, // RSHIFT
    { {      42}, {  39} }, // '*'   
    { {      47}, {  40} }, // '/'   
    { {      37}, {  41} }, // '%'   
    { {      40}, {  42} }, // '('   
    { {       0}, { -33} },          
};

SR_ s_50[] =
{
    { { REQ_DEF}, {   9} },          
    { {      43}, {  35} }, // '+'   
    { {      45}, {  36} }, // '-'   
    { {     274}, {  37} }, // LSHIFT
    { {     275}, {  38} }, // RSHIFT
    { {      42}, {  39} }, // '*'   
    { {      47}, {  40} }, // '/'   
    { {      37}, {  41} }, // '%'   
    { {      40}, {  42} }, // '('   
    { {       0}, { -34} },          
};

SR_ s_51[] =
{
    { { REQ_DEF}, {  10} },          
    { {      38}, {  32} }, // '&'   
    { {      43}, {  35} }, // '+'   
    { {      45}, {  36} }, // '-'   
    { {     274}, {  37} }, // LSHIFT
    { {     275}, {  38} }, // RSHIFT
    { {      42}, {  39} }, // '*'   
    { {      47}, {  40} }, // '/'   
    { {      37}, {  41} }, // '%'   
    { {      40}, {  42} }, // '('   
    { {       0}, { -35} },          
};

SR_ s_52[] =
{
    { { REQ_DEF}, {  11} },          
    { {      38}, {  32} }, // '&'   
    { {      94}, {  33} }, // '^'   
    { {      43}, {  35} }, // '+'   
    { {      45}, {  36} }, // '-'   
    { {     274}, {  37} }, // LSHIFT
    { {     275}, {  38} }, // RSHIFT
    { {      42}, {  39} }, // '*'   
    { {      47}, {  40} }, // '/'   
    { {      37}, {  41} }, // '%'   
    { {      40}, {  42} }, // '('   
    { {       0}, { -36} },          
};

SR_ s_53[] =
{
    { { REQ_DEF}, {   5} },       
    { {      42}, {  39} }, // '*'
    { {      47}, {  40} }, // '/'
    { {      37}, {  41} }, // '%'
    { {      40}, {  42} }, // '('
    { {       0}, { -37} },       
};

SR_ s_54[] =
{
    { { REQ_DEF}, {   5} },       
    { {      42}, {  39} }, // '*'
    { {      47}, {  40} }, // '/'
    { {      37}, {  41} }, // '%'
    { {      40}, {  42} }, // '('
    { {       0}, { -38} },       
};

SR_ s_55[] =
{
    { { REQ_DEF}, {   7} },       
    { {      43}, {  35} }, // '+'
    { {      45}, {  36} }, // '-'
    { {      42}, {  39} }, // '*'
    { {      47}, {  40} }, // '/'
    { {      37}, {  41} }, // '%'
    { {      40}, {  42} }, // '('
    { {       0}, { -39} },       
};

SR_ s_56[] =
{
    { { REQ_DEF}, {   7} },       
    { {      43}, {  35} }, // '+'
    { {      45}, {  36} }, // '-'
    { {      42}, {  39} }, // '*'
    { {      47}, {  40} }, // '/'
    { {      37}, {  41} }, // '%'
    { {      40}, {  42} }, // '('
    { {       0}, { -40} },       
};

SR_ s_57[] =
{
    { { REQ_DEF}, {   2} },       
    { {      40}, {  42} }, // '('
    { {       0}, { -41} },       
};

SR_ s_58[] =
{
    { { REQ_DEF}, {   2} },       
    { {      40}, {  42} }, // '('
    { {       0}, { -42} },       
};

SR_ s_59[] =
{
    { { REQ_DEF}, {   2} },       
    { {      40}, {  42} }, // '('
    { {       0}, { -43} },       
};

SR_ s_60[] =
{
    { { REQ_TOKEN}, {  2} },       
    { {        41}, { 65} }, // ')'
    { {         0}, {  0} },       
};

SR_ s_61[] =
{
    { { REQ_DEF}, {   2} },       
    { {      44}, {  66} }, // ','
    { {       0}, { -26} },       
};

SR_ s_62[] =
{
    { { REQ_DEF}, {  13} },          
    { {      61}, {  31} }, // '='   
    { {      38}, {  32} }, // '&'   
    { {      94}, {  33} }, // '^'   
    { {     124}, {  34} }, // '|'   
    { {      43}, {  35} }, // '+'   
    { {      45}, {  36} }, // '-'   
    { {     274}, {  37} }, // LSHIFT
    { {     275}, {  38} }, // RSHIFT
    { {      42}, {  39} }, // '*'   
    { {      47}, {  40} }, // '/'   
    { {      37}, {  41} }, // '%'   
    { {      40}, {  42} }, // '('   
    { {       0}, { -25} },          
};

SR_ s_63[] =
{
    { { REQ_TOKEN}, {  9} },              
    { {       290}, { 67} }, // expr      
    { {       260}, { 19} }, // INT       
    { {       261}, { 20} }, // RAD       
    { {       262}, { 21} }, // REAL      
    { {       263}, { 22} }, // ANGLE     
    { {       258}, { 23} }, // IDENTIFIER
    { {        45}, { 24} }, // '-'       
    { {        40}, { 25} }, // '('       
    { {         0}, {  0} },              
};

SR_ s_64[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -46} }, 
};

SR_ s_65[] =
{
    { { DEF_RED}, {   1} }, 
    { {       0}, { -47} }, 
};

SR_ s_66[] =
{
    { { REQ_TOKEN}, {  9} },              
    { {       290}, { 68} }, // expr      
    { {       260}, { 19} }, // INT       
    { {       261}, { 20} }, // RAD       
    { {       262}, { 21} }, // REAL      
    { {       263}, { 22} }, // ANGLE     
    { {       258}, { 23} }, // IDENTIFIER
    { {        45}, { 24} }, // '-'       
    { {        40}, { 25} }, // '('       
    { {         0}, {  0} },              
};

SR_ s_67[] =
{
    { { REQ_DEF}, {   2} },       
    { {      40}, {  42} }, // '('
    { {       0}, { -45} },       
};

SR_ s_68[] =
{
    { { REQ_DEF}, {  13} },          
    { {      61}, {  31} }, // '='   
    { {      38}, {  32} }, // '&'   
    { {      94}, {  33} }, // '^'   
    { {     124}, {  34} }, // '|'   
    { {      43}, {  35} }, // '+'   
    { {      45}, {  36} }, // '-'   
    { {     274}, {  37} }, // LSHIFT
    { {     275}, {  38} }, // RSHIFT
    { {      42}, {  39} }, // '*'   
    { {      47}, {  40} }, // '/'   
    { {      37}, {  41} }, // '%'   
    { {      40}, {  42} }, // '('   
    { {       0}, { -24} },          
};


// State array:
SR_ *s_state[] =
{
  s_0,  s_1,  s_2,  s_3,  s_4,  s_5,  s_6,  s_7,  s_8,  s_9,
  s_10,  s_11,  s_12,  s_13,  s_14,  s_15,  s_16,  s_17,  s_18,  s_19,
  s_20,  s_21,  s_22,  s_23,  s_24,  s_25,  s_26,  s_27,  s_28,  s_29,
  s_30,  s_31,  s_32,  s_33,  s_34,  s_35,  s_36,  s_37,  s_38,  s_39,
  s_40,  s_41,  s_42,  s_43,  s_44,  s_45,  s_46,  s_47,  s_48,  s_49,
  s_50,  s_51,  s_52,  s_53,  s_54,  s_55,  s_56,  s_57,  s_58,  s_59,
  s_60,  s_61,  s_62,  s_63,  s_64,  s_65,  s_66,  s_67,  s_68,
};

} // anonymous namespace ends



// If the parsing function call uses arguments, then provide an overloaded
// function.  The code below doesn't rely on parameters, so no arguments are
// required.  Furthermore, parse uses a function try block to allow us to do
// ACCEPT and ABORT from anywhere, even from within members called by actions,
// simply throwing the appropriate exceptions.

ParserBase::ParserBase()
:
    d_stackIdx_(-1),
    // $insert debuginit
    d_debug_(false),
    d_nErrors_(0),
    // $insert requiredtokens
    d_requiredTokens_(0),
    d_acceptedTokens_(d_requiredTokens_),
    d_token_(_UNDETERMINED_),
    d_nextToken_(_UNDETERMINED_)
{}


void Parser::print_()
{
// $insert print
}

void ParserBase::clearin()
{
    d_token_ = d_nextToken_ = _UNDETERMINED_;
}

void ParserBase::push_(size_t state)
{
    if (static_cast<size_t>(d_stackIdx_ + 1) == d_stateStack_.size())
    {
        size_t newSize = d_stackIdx_ + STACK_EXPANSION;
        d_stateStack_.resize(newSize);
        d_valueStack_.resize(newSize);
    }
    ++d_stackIdx_;
    d_stateStack_[d_stackIdx_] = d_state_ = state;
    *(d_vsp_ = &d_valueStack_[d_stackIdx_]) = d_val_;
}

void ParserBase::popToken_()
{
    d_token_ = d_nextToken_;

    d_val_ = d_nextVal_;
    d_nextVal_ = STYPE_();

    d_nextToken_ = _UNDETERMINED_;
}
     
void ParserBase::pushToken_(int token)
{
    d_nextToken_ = d_token_;
    d_nextVal_ = d_val_;
    d_token_ = token;
}
     
void ParserBase::pop_(size_t count)
{
    if (d_stackIdx_ < static_cast<int>(count))
    {
        ABORT();
    }

    d_stackIdx_ -= count;
    d_state_ = d_stateStack_[d_stackIdx_];
    d_vsp_ = &d_valueStack_[d_stackIdx_];
}

inline size_t ParserBase::top_() const
{
    return d_stateStack_[d_stackIdx_];
}

void Parser::executeAction(int production)
{
    if (d_token_ != _UNDETERMINED_)
        pushToken_(d_token_);     // save an already available token

                                    // save default non-nested block $$
    if (int size = s_productionInfo[production].d_size)
        d_val_ = d_vsp_[1 - size];

    switch (production)
    {
        // $insert actioncases
        
        case 2:
#line 38 "grammar"
        {
         d_value = (d_vsp_[-1].data<Tag_::EXPR>()).value();
         }
        break;

        case 5:
#line 52 "grammar"
        {
         resetLine();
         }
        break;

        case 10:
#line 69 "grammar"
        {
         display(d_vsp_[0].data<Tag_::EXPR>());
         }
        break;

        case 16:
#line 15 "rules/command"
        {
         quit();
         }
        break;

        case 17:
#line 20 "rules/command"
        {
         throw Exception() << "Help not yet available";
         }
        break;

        case 23:
#line 3 "rules/expr"
        {
         d_val_.get<Tag_::TYPE>() = TypeData { d_begin, d_scanner.valueType() };
         }
        break;

        case 24:
#line 10 "rules/expr"
        {
         addArg(d_vsp_[-2].data<Tag_::ARGS>(), d_vsp_[0].data<Tag_::EXPR>());
         }
        break;

        case 25:
#line 15 "rules/expr"
        {
         d_val_.get<Tag_::ARGS>() = firstArg(d_vsp_[0].data<Tag_::EXPR>());
         }
        break;

        case 27:
#line 23 "rules/expr"
        {
         d_val_.get<Tag_::ARGS>() = ArgType();
         }
        break;

        case 28:
#line 30 "rules/expr"
        {
         d_val_.get<Tag_::EXPR>() = value(Value::INT);
         }
        break;

        case 29:
#line 35 "rules/expr"
        {
         d_val_.get<Tag_::EXPR>() = value(Value::RAD);
         }
        break;

        case 30:
#line 40 "rules/expr"
        {
         d_val_.get<Tag_::EXPR>() = value(Value::REAL);
         }
        break;

        case 31:
#line 45 "rules/expr"
        {
         d_val_.get<Tag_::EXPR>() = angle();
         }
        break;

        case 32:
#line 50 "rules/expr"
        {
         d_val_.get<Tag_::EXPR>() = identifier();
         }
        break;

        case 33:
#line 55 "rules/expr"
        {
         assign(d_vsp_[-2].data<Tag_::EXPR>(), d_vsp_[0].data<Tag_::EXPR>());
         }
        break;

        case 37:
#line 66 "rules/expr"
        {
         d_val_.get<Tag_::EXPR>() = add(d_vsp_[-2].data<Tag_::EXPR>(), d_vsp_[0].data<Tag_::EXPR>());
         }
        break;

        case 41:
#line 77 "rules/expr"
        {
         d_val_.get<Tag_::EXPR>() = multiply(d_vsp_[-2].data<Tag_::EXPR>(), d_vsp_[0].data<Tag_::EXPR>());
         }
        break;

        case 44:
#line 86 "rules/expr"
        {
         d_val_.get<Tag_::EXPR>() = negate(d_vsp_[0].data<Tag_::EXPR>());
         }
        break;

        case 45:
#line 91 "rules/expr"
        {
         d_val_.get<Tag_::EXPR>() = cast(d_vsp_[-2].data<Tag_::TYPE>(), d_vsp_[0].data<Tag_::EXPR>());
         }
        break;

        case 46:
#line 96 "rules/expr"
        {
         d_val_.get<Tag_::EXPR>() = nest(d_vsp_[-1].data<Tag_::EXPR>());
         }
        break;

        case 47:
#line 101 "rules/expr"
        {
         d_val_.get<Tag_::EXPR>() = functionCall(d_vsp_[-3].data<Tag_::EXPR>(), d_vsp_[-1].data<Tag_::ARGS>());
         }
        break;

    }
}

inline void ParserBase::reduce_(PI_ const &pi)
{
    d_token_ = pi.d_nonTerm;
    pop_(pi.d_size);

}

// If d_token_ is _UNDETERMINED_ then if d_nextToken_ is _UNDETERMINED_ another
// token is obtained from lex(). Then d_nextToken_ is assigned to d_token_.
void Parser::nextToken()
{
    if (d_token_ != _UNDETERMINED_)        // no need for a token: got one
        return;                             // already

    if (d_nextToken_ != _UNDETERMINED_)
    {
        popToken_();                       // consume pending token
    }
    else
    {
        ++d_acceptedTokens_;               // accept another token (see
                                            // errorRecover())
        d_token_ = lex();
        if (d_token_ <= 0)
            d_token_ = _EOF_;
    }
    print();
}

// if the final transition is negative, then we should reduce by the rule
// given by its positive value. Note that the `recovery' parameter is only
// used with the --debug option
int Parser::lookup(bool recovery)
{
    // $insert threading
    SR_ *sr = s_state[d_state_];        // get the appropriate state-table
    int lastIdx = sr->d_lastIdx;        // sentinel-index in the SR_ array

    SR_ *lastElementPtr = sr + lastIdx;
    SR_ *elementPtr = sr + 1;            // start the search at s_xx[1]

    lastElementPtr->d_token = d_token_;// set search-token

    while (elementPtr->d_token != d_token_)
        ++elementPtr;

    if (elementPtr == lastElementPtr)   // reached the last element
    {
        if (elementPtr->d_action < 0)   // default reduction
        {
            return elementPtr->d_action;                
        }

        // No default reduction, so token not found, so error.
        throw UNEXPECTED_TOKEN_;
    }

    // not at the last element: inspect the nature of the action
    // (< 0: reduce, 0: ACCEPT, > 0: shift)

    int action = elementPtr->d_action;


    return action;
}

    // When an error has occurred, pop elements off the stack until the top
    // state has an error-item. If none is found, the default recovery
    // mode (which is to abort) is activated. 
    //
    // If EOF is encountered without being appropriate for the current state,
    // then the error recovery will fall back to the default recovery mode.
    // (i.e., parsing terminates)
void Parser::errorRecovery()
try
{
    if (d_acceptedTokens_ >= d_requiredTokens_)// only generate an error-
    {                                           // message if enough tokens 
        ++d_nErrors_;                          // were accepted. Otherwise
        error("Syntax error");                  // simply skip input

    }


    // get the error state
    while (not (s_state[top_()][0].d_type & ERR_ITEM))
    {
        pop_();
    }

    // In the error state, lookup a token allowing us to proceed.
    // Continuation may be possible following multiple reductions,
    // but eventuall a shift will be used, requiring the retrieval of
    // a terminal token. If a retrieved token doesn't match, the catch below 
    // will ensure the next token is requested in the while(true) block
    // implemented below:

    int lastToken = d_token_;                  // give the unexpected token a
                                                // chance to be processed
                                                // again.

    pushToken_(_error_);                       // specify _error_ as next token
    push_(lookup(true));                       // push the error state

    d_token_ = lastToken;                      // reactivate the unexpected
                                                // token (we're now in an
                                                // ERROR state).

    bool gotToken = true;                       // the next token is a terminal

    while (true)
    {
        try
        {
            if (s_state[d_state_]->d_type & REQ_TOKEN)
            {
                gotToken = d_token_ == _UNDETERMINED_;
                nextToken();                    // obtain next token
            }
            
            int action = lookup(true);

            if (action > 0)                 // push a new state
            {
                push_(action);
                popToken_();

                if (gotToken)
                {

                    d_acceptedTokens_ = 0;
                    return;
                }
            }
            else if (action < 0)
            {
                // no actions executed on recovery but save an already 
                // available token:
                if (d_token_ != _UNDETERMINED_)
                    pushToken_(d_token_);
 
                                            // next token is the rule's LHS
                reduce_(s_productionInfo[-action]); 
            }
            else
                ABORT();                    // abort when accepting during
                                            // error recovery
        }
        catch (...)
        {
            if (d_token_ == _EOF_)
                ABORT();                    // saw inappropriate _EOF_
                      
            popToken_();                   // failing token now skipped
        }
    }
}
catch (ErrorRecovery_)       // This is: DEFAULT_RECOVERY_MODE
{
    ABORT();
}

    // The parsing algorithm:
    // Initially, state 0 is pushed on the stack, and d_token_ as well as
    // d_nextToken_ are initialized to _UNDETERMINED_. 
    //
    // Then, in an eternal loop:
    //
    //  1. If a state does not have REQ_TOKEN no token is assigned to
    //     d_token_. If the state has REQ_TOKEN, nextToken() is called to
    //      determine d_nextToken_ and d_token_ is set to
    //     d_nextToken_. nextToken() will not call lex() unless d_nextToken_ is 
    //     _UNDETERMINED_. 
    //
    //  2. lookup() is called: 
    //     d_token_ is stored in the final element's d_token field of the
    //     state's SR_ array. 
    //
    //  3. The current token is looked up in the state's SR_ array
    //
    //  4. Depending on the result of the lookup() function the next state is
    //     shifted on the parser's stack, a reduction by some rule is applied,
    //     or the parsing function returns ACCEPT(). When a reduction is
    //     called for, any action that may have been defined for that
    //     reduction is executed.
    //
    //  5. An error occurs if d_token_ is not found, and the state has no
    //     default reduction. Error handling was described at the top of this
    //     file.

int Parser::parse()
try 
{
    push_(0);                              // initial state
    clearin();                              // clear the tokens.

    while (true)
    {
        try
        {
            if (s_state[d_state_]->d_type & REQ_TOKEN)
                nextToken();                // obtain next token


            int action = lookup(false);     // lookup d_token_ in d_state_

            if (action > 0)                 // SHIFT: push a new state
            {
                push_(action);
                popToken_();               // token processed
            }
            else if (action < 0)            // REDUCE: execute and pop.
            {
                executeAction(-action);
                                            // next token is the rule's LHS
                reduce_(s_productionInfo[-action]); 
            }
            else 
                ACCEPT();
        }
        catch (ErrorRecovery_)
        {
            errorRecovery();
        }
    }
}
catch (Return_ retValue)
{
    return retValue;
}

// $insert polymorphicImpl
namespace Meta_
{
   Base::~Base()
   {}
}




