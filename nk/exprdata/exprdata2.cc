#include "exprdata.ih"

ExprData::ExprData(Value &&tmp, size_t begin, size_t end, bool variables)
:
    d_value(std::move(tmp)),
    d_begin(begin),
    d_end(end),
    d_variables(variables)
{}
