#include "valuebase.ih"

void ValueBase::cantSet() const
{
    throw Exception() << "set(value) not available for " << typeName(d_type);
}
