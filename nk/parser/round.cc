#include "parser.ih"

void Parser::round() const
{
    cout.setf(ios::fixed, ios::floatfield);
    cout.precision(min(10UL, stoul(d_scanner.matched())));
}
