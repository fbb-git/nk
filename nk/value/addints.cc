#include "value.ih"

long long Value::addInts(Value const &lhs, Value const &rhs)
{
    return lhs.longint() + rhs.longint();
}
