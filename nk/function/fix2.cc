#include "function.ih"

double Function::fix2(Value *args, 
                      double (*basicFun)(Value const &, Value const &))
{
    require2Fixes(args);

    return basicFun(args[0], args[1]);
}
