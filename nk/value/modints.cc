#include "value.ih"

long long Value::modInts(Value const &lhs, Value const &rhs)
{
    long long rValue = rhs.longint();

    safeDenominator('%', rValue);
    return lhs.longint() % rValue;
}
