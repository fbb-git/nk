#ifndef INCLUDED_DATA_
#define INCLUDED_DATA_

#include <iosfwd>

#include "../value/value.h"

#include "../symtab/symtab.h"

class Data
{
    Symtab &d_symtab;

    public:
        Data(Symtab &symtab);
        ~Data();

        void read(std::string const &name, bool addSymtab = false) const;
        void write(std::string const &name) const;

    private:
        double toCoord(std::istream &in, double firstValue, 
                                         std::string const &hemisphere) const;

        void add(std::string const &line) const;
        std::string getName(std::istringstream &in) const;
        Value::Type getType(std::istringstream &in) const;
        Symtab::Data getValue(std::istringstream &in,
                              Value::Type type, double firstValue) const;

        std::string defaultName() const;
};
        
#endif
