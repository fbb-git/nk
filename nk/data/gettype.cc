#include "data.ih"


Value::Type Data::getType(istringstream &in) const
{
    string type;          // get the value's type name
    in >> type;

    return Value::type(type); // return the type
}
