#include "value.ih"

Value const &Value::castL2R(char const *operation, Value &lhs, Value &rhs,
                                                          Value const &right)
{
    lhs = lhs.cast(rhs.type());

    return right;
}
