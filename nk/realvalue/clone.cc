#include "realvalue.ih"

ValueBase *RealValue::clone() const
{
    return new RealValue(value());
}
