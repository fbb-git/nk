#include "parser.ih"

Parser::Parser(Symtab &symtab, istream &in)
:
    d_symtab(symtab),
    d_scanner(in),
    d_data(d_symtab),
    d_begin(0),
    d_end(0),
    d_preToken(PRE_TOKEN),
    d_backChars("dgrnesw ")      // must match s_vType[] (+ spare char)
{}
