#include "data.ih"

// values on file use the format:
//  identifier type value-as-double [another-double-for-FIX] opt-expr-text

void Data::add(string const &line) const
{
    istringstream in(line);

    string name = getName(in);
    if (name.empty())
        return;

    Value::Type type = getType(in);

    double firstValue;               // must have a value, or done
    if (!(in >> firstValue))
        return;

                                    // add the named value to the symtab
    d_symtab[name] = getValue(in, type, firstValue);
}



