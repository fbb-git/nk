#include "fixvalue.ih"

ValueBase *FixValue::clone() const
{
    return new FixValue(d_latitude, d_longitude);
}
