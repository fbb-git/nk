#include "radvalue.ih"

ostream &RadValue::insert(ostream &out) const
{
    return out << value() << 'r';
}
