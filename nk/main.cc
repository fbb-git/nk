#include "main.ih"

namespace   // the anonymous namespace can be used here
{
    Arg::LongOption longOpt[] =
    {
        Arg::LongOption("help",     'h'),
        Arg::LongOption("read",     'r'),
        Arg::LongOption("tokens",   't'),
        Arg::LongOption("version",  'v'),
        Arg::LongOption("write",    'w'),
    };
    auto longEnd = longOpt + sizeof(longOpt) / sizeof(longOpt[0]);
}

int main(int argc, char **argv)
try
{
    Arg &arg = Arg::initialize("hr::tvw::", longOpt, longEnd, argc, argv);
    arg.versionHelp(usage, version, 0);

    Symtab symtab;
    Parser parser(symtab);
//    parser.setDebug(Parser::ACTIONCASES);
    parser.setDebug(true);

    cout << "\n"
            "Welcome to NavKalk, enter /help for help, /quit to quit\n"
            "\n";

    parser.parse();
}
catch (int x)
{
    return 0;           // only used by -h and -v
}

