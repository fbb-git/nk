#include "value.ih"

double Value::addReals(Value const &lhs, Value const &rhs)
{
    return lhs.real() + rhs.real();
}
