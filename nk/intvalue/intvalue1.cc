#include "intvalue.ih"

IntValue::IntValue(long long value)
:
    ValueBase(INT),
    d_value(value)
{}
