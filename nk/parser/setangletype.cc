#include "parser.ih"

void Parser::setAngleType(Value::AngleType type, char const *text) const
{
    Value::setAngleType(type);
    cout << "Angles expected in " << text << '\n';    
}
