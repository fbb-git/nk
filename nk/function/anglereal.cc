#include "function.ih"

double Function::angle_real(Value *args, double (*basicFun)(double))
{
    Value::Type type = args->type();
    
    double argument = args->real();

    argument = Value::circleConversion(
                    type == Value::INT || type == Value::REAL ?
                        Value::angleType()
                    :
                        type, 
                    Value::RAD, 
                    argument
                );

    return basicFun(argument);
}

