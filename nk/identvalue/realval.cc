#include "identvalue.ih"

double IdentValue::realVal() const
{
    return vDeref()->real();
}
