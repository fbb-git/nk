#include "identvalue.ih"

double IdentValue::latitudeVal() const
{
    return vDeref()->latitude();
}
