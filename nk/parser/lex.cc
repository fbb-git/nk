#include "parser.ih"

int Parser::lex()
try
{
    if (d_preToken)
    {
        d_token = d_preToken;
        d_preToken = 0;
        return d_token;
    }

    d_token = d_scanner.lex();

    d_begin = d_end;
    d_end += d_scanner.length();
    d_line += d_scanner.matched();

    return d_token;
}
catch (exception const &exc)
{
    d_msg = exc.what();
    ERROR();
    return 0; // not reached
}
