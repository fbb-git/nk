#include "value.ih"

Value &Value::operator%=(Value const &right)
{
    deref().binop('%', right.deref());
    return *this;
}
