#ifndef INCLUDED_FIXVALUE_
#define INCLUDED_FIXVALUE_

#include <iosfwd>
#include "../valuebase/valuebase.h"

class FixValue: public ValueBase
{
    double d_latitude;
    double d_longitude;

    public:
        struct LatLong
        {
            double latitude;
            double longitude;
        };

        explicit FixValue(double latitude = 0, double longitude = 0);
        explicit FixValue(LatLong const &values);

    private:
        virtual double latitudeVal() const;
        virtual double longitudeVal() const;

        std::ostream &insert(std::ostream &out) const override;
        bool equals(ValueBase const &rhs) const override;

        ValueBase *clone() const override;
};

#endif





