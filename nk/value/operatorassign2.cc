#include "value.ih"

Value &Value::operator=(Value &&tmp)
{
    swap(d_value, tmp.d_value);
    return *this;
}
