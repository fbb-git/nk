#include "doublebase.ih"

double DoubleBase::longitudeVal() const
{
    return normalize(LONG, d_value);
}
