#include "parser.ih"

ExprData Parser::add(ExprData &left, ExprData &right)
{
    ExprData ret(rvalue(left) + rvalue(right), 
                left.begin(), right.end(), 
                left.variables() || right.variables());

    return ret;
}
