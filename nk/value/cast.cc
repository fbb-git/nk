#include "value.ih"

Value Value::cast(Type castType) const
{
    Value ret(deref());

    Type type = ret.type();

    if (type != castType)
        ret = factory(castType, type, ret.real());

    return ret;
}




