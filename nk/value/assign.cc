#include "value.ih"

Value &Value::assign(Value const &right)
{
    deref() = right.deref();
    return *this;
}
