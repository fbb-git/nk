#include "parser.ih"

void Parser::setDisplayRadix()
{
    d_displayRadix = stoul(d_scanner.matched().substr(1));
    Value::verifyRadix(d_displayRadix);

    cout << "Integral numbers use number system base " << d_displayRadix <<
            '\n';
}
