#include "longvalue.ih"

bool LongValue::equals(ValueBase const &rhs) const
{
    return rhs.type() == LONG && value() == rhs.longitude();
}
