#include "scanner.ih"

bool Scanner::radixNr()
{
    size_t pos = d_legalChars.find(tolower(matched()[0]));

    if (pos != string::npos)
    {
        d_radixOK = true;
        d_value *= d_radix;
        d_value += pos;
        return false;
    }        

    if (not d_radixOK)
        throw Exception() << "Value missing after " << d_radix << '\\';

    begin(StartCondition_::INITIAL);
    push(matched()[0]);

    return true;    
}

