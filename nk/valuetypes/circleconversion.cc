#include "valuetypes.ih"

double ValueTypes::circleConversion(Type fromType, Type toType, double value)
{
    bool degreeType =   toType == DEG || toType == LONG || toType == LAT;

    double fromCircle = 1;
    double toCircle = 1;

    switch (fromType)
    {
        case RAD:
            fromCircle = M_PI;
            toCircle = degreeType ? 180 : toType == GRAD ? 200 : M_PI;
        break;

        case GRAD:
            fromCircle = 200;
            toCircle = degreeType ? 180 : toType == RAD ? M_PI : 200;
        break;

        case LAT:
        case LONG:
        case DEG:
            fromCircle = 180;
            toCircle = toType == RAD ? M_PI : toType == GRAD ? 200 : 180;
        break;

        default:
        break;
    }

    return circle(fromCircle, toCircle, value);
}


