#include "value.ih"

Value &Value::operator<<=(Value const &right)
{
    deref().binop(LSHIFT, right.deref());
    return *this;
}
