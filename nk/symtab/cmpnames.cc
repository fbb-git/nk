#include "symtab.ih"

bool Symtab::cmpNames(HashtabValue const *first, 
                     HashtabValue const *second)
{
    return strcasecmp(first->first.c_str(), second->first.c_str()) < 0;
}
