#include "identvalue.ih"

ValueBase::Type IdentValue::vType() const
{
    return d_ident->value().type();
}
