#include "data.ih"

double Data::toCoord(istream &in, double firstValue, string const &hemisphere)
                    const
{
    bool neg = firstValue < 0;
    if (neg)
        firstValue = -firstValue;

    string spec = to_string(static_cast<int>(firstValue));

    char ch;
    while (in.get(ch))
    {
        spec += ch;
        if (hemisphere.find(ch) != string::npos)
            break;
    }

    firstValue = Value::degSpec3(spec);
    if (neg or spec.find_last_of("SW") != string::npos)
        firstValue = -firstValue;

    return firstValue;
}
