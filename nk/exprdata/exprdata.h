#ifndef INCLUDED_EXPRDATA_
#define INCLUDED_EXPRDATA_

#include "../value/value.h"
#include "../symtab/symtab.h"

class ExprData
{
    std::string d_ident;        // with cons 3
    Symtab::Data *d_symData = 0;
    Value d_value;
    size_t d_begin;
    size_t d_end;
    bool   d_variables;
    
    public:
        ExprData() = default;
        ExprData(Value &&tmp, size_t begin, size_t end, bool variables);
        ExprData(std::string const &ident, Symtab &symtab, 
                 size_t begin, size_t end);

        size_t begin() const;
        size_t end() const;
        size_t length() const;

        Value const &value() const;
        Symtab::Data &symData() const;
        bool variables() const;

        Value::Type type() const;
        std::string const &ident() const;

        void assign(ExprData const &rhs);
        void append(ExprData const &rhs);

        Value &value();
        void begin(size_t newBegin);
        void end(size_t newEnd);
};

inline std::string const &ExprData::ident() const
{
    return d_ident;
}

inline Value const &ExprData::value() const
{
    return d_symData ? d_symData->value() : d_value;
}

inline Value &ExprData::value() 
{
    return d_symData ? d_symData->value() : d_value;
}

inline Symtab::Data  &ExprData::symData() const
{
    return *d_symData;
}

inline Value::Type ExprData::type() const
{
    return d_value.type();
}

inline size_t ExprData::begin() const
{
    return d_begin;
}

inline size_t ExprData::end() const
{
    return d_end;
}

inline size_t ExprData::length() const
{
    return d_end - d_begin;
}

inline bool ExprData::variables() const
{
    return d_variables;
}

inline void ExprData::begin(size_t newBegin)
{
    d_begin = newBegin;
}

inline void ExprData::end(size_t newEnd)
{
    d_end = newEnd;
}


#endif






