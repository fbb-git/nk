#include "symtab.ih"

Symtab::Data &Symtab::operator[](std::string const &name)
{
    if (d_entry[name] == 0)
        d_entry[name].reset(new Data);

    return *d_entry[name];
}
