#ifndef INCLUDED_FUNBASE_
#define INCLUDED_FUNBASE_

#include <string>
#include <unordered_map>

class Value;

class FunBase
{
    std::string const d_name;
    size_t d_arity;

    public:
        FunBase(std::string const &name, size_t arity);
        virtual ~FunBase();

        Value operator()(size_t nArgs, Value *args) const;

        static double distance(Value const &from, Value const &to);

    protected:
        static double differenceCoord(double first, double second);

        void require2Fixes(Value const*args) const;

    private:
        virtual Value call(Value *args) const = 0;
};

#endif








