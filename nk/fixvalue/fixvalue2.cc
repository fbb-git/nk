#include "fixvalue.ih"

FixValue::FixValue(LatLong const &values)
:
    ValueBase(FIX),
    d_latitude(values.latitude),
    d_longitude(values.longitude)
{}
