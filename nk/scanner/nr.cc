#include "scanner.ih"

int Scanner::nr(size_t radix)
{
    string const &str = radix == 16 || radix == 2 ? 
                            matched().substr(2) : matched();

    d_value = stoull(str, 0, radix);

    return matched().back() == 'r' ? Parser::RAD : Parser::INT;
}

