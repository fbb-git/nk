#include "function.ih"

double Function::real2(Value *args, double (*basicFun)(double, double))
{
    return basicFun(args[0].real(), args[1].real());
}
