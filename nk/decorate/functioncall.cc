#include "parser.ih"

ExprData Parser::functionCall(ExprData const &fun, ArgType &args)
{
//    cout << "FUNCALL " << d_line.substr(fun.begin(), d_end - fun.begin()) <<
//            '\n';

    return ExprData(d_symtab(fun.ident(), args.arg.size(), &args.arg[0]), 
                    fun.begin(), d_end, args.variables);

// if 
// (
//     ret.type() == Value::RAD && 
//     Value::angleType() != Value::RAD
// )
//     ret = Value::factory(
//                 Value::angleType(), 
//                 Value::circleConversion(Value::RAD,
//                                         Value::angleType(), 
//                                         ret.real())
//             );
                    
}
