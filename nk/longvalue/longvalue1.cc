#include "longvalue.ih"

LongValue::LongValue(double value)
:
    DoubleBase(LONG, value)
{}
