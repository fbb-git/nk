#include "fixvalue.ih"

ostream &FixValue::insert(ostream &out) const
{
    toLat(out, d_latitude) << ' ';
    return toLong(out, d_longitude);
}
