#include "function.ih"

double Function::distance(Value const &from, Value const &to)
{
                                        // complement of latitude
    double fromComp    = Value::d2r(90 - from.latitude());  
    double toComp      = Value::d2r(90 - to.latitude());
    double deltaLong   = Value::d2r(
                            differenceCoord(from.longitude(), to.longitude())
                        );

    return 60 * Value::r2d(
                    acos(
                        cos(fromComp) * cos(toComp) +
                        sin(fromComp) * sin(toComp) * cos(deltaLong)
                    )
                );
}








