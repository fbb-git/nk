#include "valuetypes.ih"

ValueTypes::Type ValueTypes::type(string const &target)
{
    auto iter = find(s_typeName, s_typeName + s_sizeofTypeName, target);

    if (iter == s_typeName + s_sizeofTypeName)
        throw Exception() << "internal error: ValueTypes::type(" << target << 
                        ") requested";

    return static_cast<ValueTypes::Type>(iter - s_typeName);
}

