#include "value.ih"

Value &Value::operator=(Value const &rhs)
{
    Value tmp(rhs);
    swap(d_value, tmp.d_value);

    return *this;
}
