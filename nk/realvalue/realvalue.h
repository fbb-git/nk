#ifndef INCLUDED_REALVALUE_
#define INCLUDED_REALVALUE_

#include <iosfwd>

#include "../doublebase/doublebase.h"

class RealValue: public DoubleBase
{
    public:
        explicit RealValue(double value = 0);   // 1 default constructor

    private:

        std::ostream &insert(std::ostream &out) const override;
        bool equals(ValueBase const &rhs) const override;
        ValueBase *clone() const override;
};

#endif





