#include "function.ih"

void Function::require2Fixes(Value const *args)
{
    if 
    (
        args[0].derefType() != Value::FIX 
        or 
        args[1].derefType() != Value::FIX
    )
        throw Exception() << "FIX arguments required";
}
