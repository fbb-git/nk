#include "value.ih"

double Value::modReals(Value const &lhs, Value const &rhs)
{
    double rValue = rhs.real();

    safeDenominator('%', rValue);

    return fmod(lhs.real(), rValue);
}
