#include "valuetypes.ih"

char const *ValueTypes::s_typeName[] =
{
    "INT",
    "REAL",
    "DEG",
    "RAD",
    "GRAD",
    "LAT",
    "LONG",
    "FIX",
    "IDENT",
};

string const ValueTypes::s_numberChars(
            "0123456789"    "abcdefghijklmnopqrstuvwxyz"
        );

ValueTypes::Type ValueTypes::s_angle = ValueTypes::RAD;

