#ifndef INCLUDED_DOUBLEBASE_
#define INCLUDED_DOUBLEBASE_

#include <iosfwd>
#include "../valuebase/valuebase.h"

class DoubleBase: public ValueBase
{
    double d_value;

    protected:
        explicit DoubleBase(Type type, double value); // 1 default constructor
        double value() const;

    private:
        void vNegate() override;

        long long intVal() const override;
        double realVal() const override;  
        double latitudeVal() const override;
        double longitudeVal() const override;

        void vSet(Type,      long long value) override;
        void vSet(Type type, double value) override;
};

#endif






