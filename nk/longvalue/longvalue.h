#ifndef INCLUDED_LONGVALUE_
#define INCLUDED_LONGVALUE_

#include <iosfwd>
#include "../doublebase/doublebase.h"

class LongValue: public DoubleBase
{
    public:
        explicit LongValue(double value = 0);   // 1 default constructor

    private:
        using ValueBase::latitudeVal;           // these are now suppressed

        std::ostream &insert(std::ostream &out) const override;
        bool equals(ValueBase const &rhs) const override;

        ValueBase *clone() const override;
};

#endif





