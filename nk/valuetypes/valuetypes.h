#ifndef INCLUDED_VALUETYPES_
#define INCLUDED_VALUETYPES_

#include <iosfwd>
#include <string>
#include <cmath>

struct ValueTypes
{
    enum Type       // modify data.cc when this enum changes
    {
        INT,
        REAL,
        DEG,
        RAD,
        GRAD,
        LAT,        // value stored as degrees
        LONG,       // value stored as degrees
        FIX,
        IDENT,
    };

    private:
        static char const *s_typeName[];
        static std::string const s_numberChars;
        static Type s_angle;

    protected:
        static size_t const s_sizeofTypeName = static_cast<int>(IDENT) + 1;
        static double constexpr s_minimum = 1e-8;

    public:
        enum class AngleType
        {
            DEG = Type::DEG,
            RAD = Type::RAD,
            GRAD = Type::GRAD
        };

        static Type angleType();
        static void setAngleType(AngleType angle);

        static char const *typeName(Type type);
        static Type type(std::string const &target);
        static bool isZero(double value);

        static double circleConversion(Type fromType, Type toType, 
                                       double value);

        static size_t maxRadix();
        static std::string numberChars(size_t radix);
        static char numberChar(size_t value);
        static void verifyRadix(size_t radix);

        static double constexpr d2r(double value);
        static double constexpr r2d(double value);

    protected:
        static double normalize(Type type, double value); 

    private:
        static double maxValue(double max, double value);
        static double constexpr circle(double fromCircle, double toCircle,
                                       double value);
};

inline double constexpr ValueTypes::circle(double fromCircle, double toCircle,
                                           double value) 
{
    return value * toCircle / fromCircle;
}

inline ValueTypes::Type ValueTypes::angleType()
{
    return s_angle;
}

inline void ValueTypes::setAngleType(AngleType angle)
{
    s_angle = static_cast<Type>(angle);
}

inline size_t ValueTypes::maxRadix()
{
    return s_numberChars.length();
}

inline char ValueTypes::numberChar(size_t value)
{
    return s_numberChars[value];
}

inline double constexpr ValueTypes::d2r(double value)
{
    return circle(180, M_PI, value);
}

inline double constexpr ValueTypes::r2d(double value)
{
    return circle(M_PI, 180, value);
}


#endif
