#include "scanner.ih"

Parser::Tokens_ Scanner::type(Value::Type valueType)
{
    d_valueType = valueType;
    return Parser::TYPE;
}
