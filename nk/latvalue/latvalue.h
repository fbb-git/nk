#ifndef INCLUDED_LATVALUE_
#define INCLUDED_LATVALUE_

#include <iosfwd>
#include "../doublebase/doublebase.h"

class LatValue: public DoubleBase
{
    public:
        explicit LatValue(double value = 0);   // 1 default constructor

    private:
        using ValueBase::longitudeVal;

        std::ostream &insert(std::ostream &out) const override;
        bool equals(ValueBase const &rhs) const override;

        ValueBase *clone() const override;
};

#endif





