#include "parser.ih"

void Parser::dontRound()
{
    cout.unsetf(ios::fixed);
    cout.precision(6);
}
