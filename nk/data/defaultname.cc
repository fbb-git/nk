#include "data.ih"

string Data::defaultName() const
{
    Arg &arg = Arg::instance();

    string name;

    if (not arg.option(&name, 'w') || name.empty())
    {
        name = getenv("HOME");
        name += "/.navrc";
    }

    return name;
}
