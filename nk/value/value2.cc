#include "value.ih"

Value::Value(Value &&tmp)
:
    d_value(tmp.d_value)
{
    tmp.d_value = 0;
}
