#include "value.ih"

Value Value::operator-() const
{
    Value ret(deref());

    ret.d_value->negate();

    return ret;
}
