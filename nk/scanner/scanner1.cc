#include "scanner.ih"

Scanner::Scanner(std::istream &in, std::ostream &out)
:
    ScannerBase(in, out),
    d_printTokens(Arg::instance().option('t'))
{}

