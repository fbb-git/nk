#ifndef INCLUDED_DEGVALUE_
#define INCLUDED_DEGVALUE_

#include <iosfwd>
#include "../doublebase/doublebase.h"

class DegValue: public DoubleBase
{
    double d_value;

    public:
        explicit DegValue(double value = 0);   // 1 default constructor

    private:

        std::ostream &insert(std::ostream &out) const override;
        bool equals(ValueBase const &rhs) const override;

        using ValueBase::latitudeVal;           // these are now suppressed
        using ValueBase::longitudeVal;

        ValueBase *clone() const override;

};

#endif





