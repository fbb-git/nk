#include "data.ih"

string Data::getName(istringstream &in) const
{
    string name;
    in >> name;
                                // name must start with alpha or underscore
    if (not (in && (isalpha(name[0]) || name[0] == '_')))
        name.clear();

    return name;
}
