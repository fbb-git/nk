#include "symtab.ih"

std::ostream &operator<<(std::ostream &out, Symtab::HashtabValue const &value)
{
    return out << value.first << ' ' << 
                    value.second->value().typeName() << ' ' << 
                    value.second->value() << ' ' <<
                    value.second->exprText();
}

