#ifndef INCLUDED_VALUE_
#define INCLUDED_VALUE_

#include <iosfwd>
#include <unordered_map>
#include <string>

#include "../valuetypes/valuetypes.h"

// Value is a wrapper class for ValueBase. The actual values are polymorphic,
//  and Value performs the required dereferencing and cleanup when ceasing to
//  exist

class ValueBase;

class Value: public ValueTypes
{
    friend std::ostream &operator<<(std::ostream &out, Value const &value);
    friend bool operator==(Value const &lhs, Value const &rhs);

    struct CastStruct
    {
        typedef Value const &(*CastFun)(char const *operation, Value &lhs, 
                                        Value &rhsCast, Value const &rhs);
        char const *operation;
        long long (*intOp)(Value const &lhs, Value const &rhs);
        double (*realOp)(Value const &lhs, Value const &rhs);
        CastFun const castSquare[s_sizeofTypeName][s_sizeofTypeName];
    };
        
    ValueBase *d_value;

    static std::unordered_map<int, CastStruct> s_cast;

    enum Operators
    {
        LSHIFT = 256,
        RSHIFT
    };

    public:
        Value(Value const &other);              // 1
        Value(Value &&tmp);                     // 2
        Value();                                // 3
        Value(ValueBase *valuebase);            // 4:   ptr owned by Value

        ~Value();

        Value &operator=(Value const &rhs);     // 1
        Value &operator=(Value &&tmp);          // 2

        void set(double value);                 // 1
        void set(long long value);              // 2

        long long longint() const;
        double real() const;
        double longitude() const;
        double latitude() const;

        bool isLvalue() const;

        using ValueTypes::type;
        Type type() const;
        char const *typeName() const;
        Type derefType() const;

        ValueBase const &valueBase() const;

        Value cast(Type type) const;          // conform C++'s cast
        Value operator-() const;

        Value &operator+=(Value const &other);
        Value &operator-=(Value const &other);
        Value &operator*=(Value const &other);
        Value &operator/=(Value const &other);
        Value &operator%=(Value const &other);
        Value &operator<<=(Value const &other);
        Value &operator>>=(Value const &other);
        Value &operator&=(Value const &other);
        Value &operator|=(Value const &other);
        Value &operator^=(Value const &other);

        Value &assign(Value const &rhs);    // cannot use op= here
                // as assign must dereference IdentValue objects

        static Value factory(Type destType, Type fromType, double value);
        static Value factory(Type destType, double value);
        static Value factory(double longitude, double latitude);

        static double degSpec2(std::string const &matched);
        static double degSpec3(std::string const &matched);

    private:
        using ValueTypes::typeName;

        Value &deref();
        Value const &deref() const;

        void binop(int operation, Value const &right);

        static Value const &ok(char const *operation, 
                                Value &lhs, Value &rhsCast, 
                                Value const &rhs);        
        static Value const &no(char const *operation, 
                                Value &lhs, Value &rhsCast, 
                                Value const &rhs);        
        static Value const &castR2L(char const *operation, 
                                Value &lhs, Value &rhsCast, 
                                Value const &rhs);        
        static Value const &castL2R(char const *operation, 
                                Value &lhs, Value &rhsCast, 
                                Value const &rhs);

        static long long andInts(Value const &lhs, Value const &rhs);

        static long long orInts(Value const &lhs, Value const &rhs);

        static long long xorInts(Value const &lhs, Value const &rhs);

        static long long lshiftInts(Value const &lhs, Value const &rhs);

        static long long rshiftInts(Value const &lhs, Value const &rhs);

        static long long addInts(Value const &lhs, Value const &rhs);
        static double addReals(Value const &lhs, Value const &rhs);

        static long long subInts(Value const &lhs, Value const &rhs);
        static double subReals(Value const &lhs, Value const &rhs);

        static long long mulInts(Value const &lhs, Value const &rhs);
        static double mulReals(Value const &lhs, Value const &rhs);

        static long long divInts(Value const &lhs, Value const &rhs);
        static double divReals(Value const &lhs, Value const &rhs);

        static long long modInts(Value const &lhs, Value const &rhs);
        static double modReals(Value const &lhs, Value const &rhs);

        static void safeDenominator(char operation, double value);
};

inline Value Value::factory(Type destType, double value)
{  
    return factory(destType, destType, value);
}


//inline Value operator-(Value const &lhs, Value const &rhs)
//{
//    Value ret(lhs);
//    ret -= rhs;
//    return ret;
//}
//
//inline Value operator+(Value const &lhs, Value const &rhs)
//{
//    Value ret(lhs);
//    ret += rhs;
//    return ret;
//}

#endif
