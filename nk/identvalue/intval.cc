#include "identvalue.ih"

long long IdentValue::intVal() const
{
    return vDeref()->longint();
}
