#include "parser.ih"

void Parser::addArg(ArgType &args, ExprData &expr)
{
//    Value ret = rvalue(expr);         // doesn't work? Weird...
//    args.arg.push_back(ret);

    vector<Value> vv{ move(args.arg) };
    vv.resize(vv.size() + 1);
    vv[vv.size() - 1] = rvalue(expr);

    args.arg = move(vv);

    args.variables  |= expr.variables();
}
