#include "function.ih"

double Function::real(Value *args, double (*basicFun)(double))
{
    return basicFun(args->real());
}

