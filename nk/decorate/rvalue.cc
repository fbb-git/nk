#include "parser.ih"

Value Parser::rvalue(ExprData &exprData)
{
    Value ret;

    if (exprData.type() != Value::IDENT)
        ret = exprData.value();
    else
    {
        Symtab::Data &symData = exprData.symData();
        if (symData.nested() || not symData.hasExprText())
            ret = symData.value();
        else
        {
            istringstream exprStream(symData.exprText());
            Parser parser(d_symtab, exprStream);
            symData.pushNested();
        cout << "RVALUE parsing: " << symData.exprText() << '\n';
            parser.parse();
            symData.popNested();
            ret = parser.value();
        }
    }
    return ret;
}       
