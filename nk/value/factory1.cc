#include "value.ih"

Value Value::factory(Type destType, Type fromType, double value)
{
    ValueBase *vb;

    switch (destType)
    {
        case INT:
            vb = new IntValue(value);
        break;

        case REAL:
            vb = new RealValue(value);
        break;

        case RAD:
            vb = new RadValue(circleConversion(fromType, RAD, value));
        break;
     
        case GRAD:
            vb = new GradValue(circleConversion(fromType, GRAD, value));
        break;
     
        case DEG:
            vb = new DegValue(circleConversion(fromType, DEG, value));
        break;
     
        case LAT:
            vb = new LatValue(circleConversion(fromType, LAT, value));
        break;
     
        case LONG:
            vb = new LongValue(circleConversion(fromType, LONG, value));
        break;
     
        default:
        throw logic_error("improper type for Value::factory(Type, ...)");
    }

    Value ret(vb);

    return ret;
}





