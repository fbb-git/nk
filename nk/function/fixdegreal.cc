#include "function.ih"

FixValue::LatLong Function::fixDegReal(Value *args, 
                    FixValue::LatLong (*fun)(double, double, double, double))
{
    return (*fun)(args[0].latitude(), args[0].longitude(), 
        Value::circleConversion(args[1].type(), Value::RAD, args[1].real()), 
        args[2].real());
}
