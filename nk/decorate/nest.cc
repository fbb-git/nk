#include "parser.ih"

ExprData Parser::nest(ExprData &expr)
{
    ExprData ret(expr);
    ret.begin(expr.begin() - 1);
    ret.end(expr.end() + 1);
    
    return ret;
}
