#include "valuetypes.ih"

bool ValueTypes::isZero(double value)
{
    return fabs(value) < s_minimum;
}

