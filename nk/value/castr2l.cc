#include "value.ih"

Value const &Value::castR2L(char const *operation, Value &lhs, Value &rhs,
                                                          Value const &right)
{
    rhs = right.cast(lhs.type());
    return rhs;
}
