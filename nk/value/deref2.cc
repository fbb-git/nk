#include "value.ih"

Value const &Value::deref() const
{   
    return *(isLvalue() ? d_value->deref() : this);
}
