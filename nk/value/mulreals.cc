#include "value.ih"

double Value::mulReals(Value const &lhs, Value const &rhs)
{
    return lhs.real() * rhs.real();
}
