#ifndef INCLUDED_INTVALUE_
#define INCLUDED_INTVALUE_

#include <iosfwd>
#include "../valuebase/valuebase.h"

class IntValue: public ValueBase
{
    long long d_value;

    public:
        explicit IntValue(long long value = 0);   // 1 default constructor

    private:
        void vNegate() override;

        void vSet(Type, long long value) override;  // Type ignored
        void vSet(Type, double value) override;    

        long long intVal() const override;
        double realVal() const override;  

        std::ostream &insert(std::ostream &out) const override;
        bool equals(ValueBase const &rhs) const override;

        ValueBase *clone() const override;
};

#endif










