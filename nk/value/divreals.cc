#include "value.ih"

double Value::divReals(Value const &lhs, Value const &rhs)
{
    double rValue = rhs.real();

    safeDenominator('/', rValue);

    return lhs.real() / rValue;
}
