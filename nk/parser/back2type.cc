#include "parser.ih"

Parser::ValueStruct Parser::back2Type()
{
    int last = tolower(d_scanner.matched().back());

//    cout << "last char = " << (char)last << '\n';

    d_backChars.back() = last;

//    cout << "backchars: " << d_backChars << ", idx = " << 
//            d_backChars.find(last) << '\n';

    return  s_vType[d_backChars.find(last)];
}
