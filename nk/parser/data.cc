#include "parser.ih"

ReadLineStream Parser::s_in("nk> ", 20,  ReadLineBuf::EXPAND_HISTORY);

Parser::ValueStruct Parser::s_vType[] =   // must match d_backChars
{
    {Value::DEG,    false},  // d
    {Value::GRAD,   false},  // g
    {Value::RAD,    false},  // r
    {Value::LAT,    false},  // n
    {Value::LONG,   false},  // e
    {Value::LAT,    true},   // s
    {Value::LONG,   true},   // w
    {Value::DEG,    false},  // default
};

