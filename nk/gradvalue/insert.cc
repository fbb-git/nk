#include "gradvalue.ih"

ostream &GradValue::insert(ostream &out) const
{
    return toDegMinSec(out, value()) << 'g';
}
