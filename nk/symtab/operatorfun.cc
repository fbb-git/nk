#include "symtab.ih"

Value Symtab::operator()(string const &name, size_t nArgs, Value *args) const
{
    return d_function(name, nArgs, args);
}
