#include "degvalue.ih"

ostream &DegValue::insert(ostream &out) const
{
    toDegMinSec(out, value());

    return out;
}
