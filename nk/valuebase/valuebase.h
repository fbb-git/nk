#ifndef INCLUDED_VALUEBASE_
#define INCLUDED_VALUEBASE_

#include <iosfwd>
#include <unordered_map>

#include "../valuetypes/valuetypes.h"

class Value;
class ValueBase: public ValueTypes 
{
    friend bool operator==(ValueBase const &lhs, ValueBase const &rhs);
    friend std::ostream &operator<<(std::ostream &out, 
                                    ValueBase const &valuebase);

    Type d_type;

    public:
        virtual ~ValueBase();

        ValueBase *copy() const;

        void set(Type, long long value);    // 1: Type is not used, but 
                                            // requiring it prevents 
                                            // accidenally setting an INT 
                                            // value

        void set(Type type, double value);  // 2: DoubleBase needs the type

        Type type() const;
        Type derefType() const;

        long long longint() const;

        double real() const;
        double longitude() const;
        double latitude() const;

        void negate();

        Value *deref();
        Value const *deref() const;

    protected:
        explicit ValueBase(Type type);   
        ValueBase(ValueBase const &other) = default;    // called by derived
                                                // classes to define their 
                                                // types

                                    // in the protected interface so they
                                    // can be used by Deg-, Long-, Lat- and 
                                    // FixValue
        static std::ostream &toDegMinSec(std::ostream &out, double value);
        static std::ostream &toLat(std::ostream &out, double latitude);
        static std::ostream &toLong(std::ostream &out, double longitude);

            // these are protected to allow subclasses that don't offer
            // these members to use them (thru 'using ...'), thereby
            // throwing an exception when called
        virtual double latitudeVal() const;
        virtual double longitudeVal() const;

    private:
            // all default virtual implementations throw 'unavailable' 
            // Exceptions
        virtual void vSet(Type,      long long value);
        virtual void vSet(Type type, double value);
        void cantSet() const;

        virtual void vNegate();
        virtual Value *vDeref() const;
        virtual long long intVal() const;
        virtual double realVal() const;  
        void wrongType(char const *type) const;

        virtual Type vType() const;

        virtual ValueBase *clone() const = 0;
        virtual std::ostream &insert(std::ostream &out) const = 0;
        virtual bool equals(ValueBase const &rhs) const = 0;
};

#endif
