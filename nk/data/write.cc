#include "data.ih"

void Data::write(std::string const &name) const
{
    ofstream out(name.empty() ? defaultName() : name);

    if (!out)
        throw Exception() << "Can't write `" << name << "'";

    for (auto &entry: d_symtab)
    {
        if (not d_symtab.isFunction(entry))
            out << entry << '\n';
    }
}








