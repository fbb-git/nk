#include "exprdata.ih"

ExprData::ExprData(string const &ident, Symtab &symtab, 
                   size_t begin, size_t end)
:
    d_ident(ident),
    d_symData(&symtab[ident]),
    d_value(new IdentValue(d_symData)),
    d_begin(begin),
    d_end(end),
    d_variables(true)
{}
