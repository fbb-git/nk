#include "value.ih"

char const *Value::typeName() const
{
    return ValueTypes::typeName(type());
}
