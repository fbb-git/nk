#ifndef INCLUDED_FUNCTION_
#define INCLUDED_FUNCTION_

#include <string>
#include <unordered_map>

#include "../funwrap/funwrap.h"
#include "../fixvalue/fixvalue.h"

//class Value;
#include "../value/value.h"

// see expr/functioncall.cc for details

class Function
{
    template <typename ValueType, typename Converter, typename BasicFun>
    class Handler: public FunBase
    {
        Converter d_converter;
        BasicFun d_basicFun;

        public:
            Handler(char const *name, size_t arity, Converter converter, 
                    BasicFun basicFun);
        private:
            Value call(Value *args) const override;
    };

    typedef std::unordered_map<std::string, FunWrap> FunMap;

    static FunMap const s_function;

    public:
        Value operator()(std::string const &name, size_t nArgs, Value *args)
                                                                        const;
        static bool isFunction(std::string const &name);

    private:

        template <typename ValueType, typename Converter, typename BasicFun>
        static FunBase *makeHandler(char const *name, size_t arity,
                                    Converter converter, BasicFun basicFun);
                                    
        static double angle_real(Value *args, double (*basicFun)(double));
        static double real(Value *args, double (*basicFun)(double));
        static double real2(Value *args, 
                                double (*basicFun)(double, double));
        static FixValue::LatLong fixDegReal(Value *args, 
                        FixValue::LatLong (*fun)(
                                               double, double, double, double)
                                            );


        static void require2Fixes(Value const *args);
        static double distance(Value const &from, Value const &to);
        static double heading(Value const &from, Value const &to);

        static FixValue::LatLong latLong(Value const *args, int);
        static double fix2(Value *args, 
                           double (*basicFun)(Value const &, Value const &));
        static double differenceCoord(double first, double second);
        static FixValue::LatLong deadReckon(double latitude, double longitude,
                                double headingRad, double distanceNM);
};

inline bool Function::isFunction(std::string const &name)
{
    return s_function.find(name) != s_function.end();
}
        
template <typename ValueType, typename Converter, typename BasicFun>
Function::Handler<ValueType, Converter, BasicFun>::Handler(
    char const *name, size_t arity, Converter converter, BasicFun basicFun
)
:
    FunBase(name, arity),
    d_converter(converter),
    d_basicFun(basicFun)
{}

template <typename ValueType, typename Converter, typename BasicFun>
inline Value Function::Handler<ValueType, Converter, BasicFun>::call(
                                                Value *args) const
{
    return new ValueType(d_converter(args, d_basicFun));
}

template <typename ValueType, typename Converter, typename BasicFun>
inline FunBase *Function::makeHandler(char const *name, size_t arity,
                                      Converter converter, BasicFun basicFun)
{
    return new Handler<ValueType, Converter, BasicFun>
                    (name, arity, converter, basicFun);
}
                                    
#endif
