#include "funbase.ih"

FunBase::FunBase(string const &name, size_t arity)
:
    d_name(name),
    d_arity(arity)
{}
