#include "intvalue.ih"

bool IntValue::equals(ValueBase const &rhs) const
{
    return rhs.type() == INT && d_value == rhs.longint();
}
