#include "latvalue.ih"

bool LatValue::equals(ValueBase const &rhs) const
{
    return rhs.type() == LAT && value() == rhs.latitude();
}
