#include "value.ih"

long long Value::subInts(Value const &lhs, Value const &rhs)
{
    return lhs.longint() - rhs.longint();
}
