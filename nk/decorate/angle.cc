#include "parser.ih"

ExprData Parser::angle()
{
    ValueStruct const &vStruct = back2Type();
    double value = d_scanner.value();
    return ExprData(
                Value::factory(vStruct.type, vStruct.neg ? -value : value),
                d_begin,
                d_end,
                false
            );
}
