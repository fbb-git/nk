#include "valuebase.ih"

ostream &ValueBase::toDegMinSec(ostream &out, double value)
{
    bool neg = value < 0;

    if (neg)
    {
        out << '-';
        value = -value;
    }
    size_t sec = round(value * 3600);
    size_t min = sec / 60;
    sec %= 60;
    size_t deg = min / 60;
    min %= 60;

    return out << deg << ':' << min << ':' << sec;
}
