#include "valuebase.ih"

ostream &ValueBase::toLong(ostream &out, double longitude)
{
    return toDegMinSec(out, fabs(longitude)) << ' ' << 
                    (longitude >= 0 ? 'E' : 'W');
}
