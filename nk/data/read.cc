#include "data.ih"

void Data::read(string const &name, bool addSymtab) const
{
    ifstream in(name.empty() ? defaultName() : name);

    if (!in)
        throw Exception() << "Can't read `" << name << "'";

    if (not addSymtab)
        d_symtab.clear();

    string line;
    while (getline(in, line))
        add(line);        
}
