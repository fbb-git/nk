#include "fixvalue.ih"

bool FixValue::equals(ValueBase const &rhs) const
{
    return rhs.type() == FIX && 
           d_latitude == rhs.latitude() && d_longitude == rhs.longitude();
}
