#include "doublebase.ih"

DoubleBase::DoubleBase(Type type, double value)
:
    ValueBase(type),
    d_value(normalize(type, value))
{}
