#ifndef INCLUDED_ARGTYPE_H_
#define INCLUDED_ARGTYPE_H_

#include <vector>
#include "../value/value.h"

struct ArgType
{
    std::vector<Value> arg;
    bool               variables = false;
};

#endif
