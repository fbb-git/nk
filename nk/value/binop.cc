#include "value.ih"

void Value::binop(int operation, Value const &right)
{
                                    // obtain the appropriate set of casts
    CastStruct const &castStruct = s_cast.find(operation)->second;

                        // perform a cast if necessary
    Value rhsCast;      // Value holding rhs after casting
    Value const &rhs =  // rhs to use: either right or rhsCast
            castStruct.castSquare[type()][right.type()]
                              (castStruct.operation, *this, rhsCast, right);

    // at this point lhs holds the value of the required return type
    // how to operate lhs and rhs (e.g., lhs += rhs) ?
    // the values to operate on are either ints or doubles, and lhs is already
    // at its required type. Simply set their values to the operation's 
    // result should do the trick

    Type tp = type();

    if (tp == INT)
        set(castStruct.intOp(*this, rhs));
    else
        set(castStruct.realOp(*this, rhs));
}

