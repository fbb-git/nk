#include "gradvalue.ih"

bool GradValue::equals(ValueBase const &rhs) const
{
    return rhs.type() == GRAD && value() == rhs.real();
}
