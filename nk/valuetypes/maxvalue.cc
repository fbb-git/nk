#include "valuetypes.ih"

double ValueTypes::maxValue(double max, double value)
{
    return fabs(value) > max ? fmod(value, max) : value;
}
