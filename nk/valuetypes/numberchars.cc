#include "valuetypes.ih"

string ValueTypes::numberChars(size_t radix)
{
    verifyRadix(radix);
    return s_numberChars.substr(0, radix);
}
