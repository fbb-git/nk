#include "valuetypes.ih"

double ValueTypes::normalize(Type type, double value)
{
    switch (type)
    {
        case DEG:
        return fmod(value, 360.0);

        case RAD:
        return fmod(value, 2 * M_PI);

        case GRAD:
        return fmod(value, 400.0);

        case LONG:
        return maxValue(180.0, value);

        case LAT:
        return maxValue(90.0, value);
        break;

        default:
        return value;
    }
}
